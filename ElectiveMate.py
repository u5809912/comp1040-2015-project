__author__ = 'nsifniotis'

import json
from DataModel.Course import Course
from Scraper.DataRepo import Repository

"""
Nick Sifniotis u5809912
29/10/2015

ElectiveMate

A simple utility that provides a list of electives that ANU students can take
based on the courses they have successfully completed.

"""

repo = Repository("")

# step 1 - get input from the user
# a GUI would be better but there may be no time to implement it
print("ElectiveMate")
print()
print("Please enter the course codes of courses you have completed at ANU.")
print("Enter them one at a time. To finish, enter a blank line.")
print()

list_of_courses_completed = []
course_counter = 1
finished = False
while not finished:
    next_course = input("Course code " + str(course_counter) + ": ").upper()
    if next_course == "":
        finished = True
    else:
        item_type, item = repo.identify_me(next_course, "CRS")
        if item_type != "CRS":
            print("Sorry, but the course code " + next_course + " is not recognised. Please check and try again.")
        else:
            print("Course " + item.course_code + " (" + item.course_name + ") accepted.")
            list_of_courses_completed.append(item)
            course_counter += 1

# step 2 - process the input and generate the list of electives

# subtask - load the complete course data
# for now because of time pressure, stick to CS courses only.
courses = []
with open('ParsedData/Courses.json') as list_file:
    list_data = json.load(list_file)
for course_code, course_data in list_data.items():
    if course_data['subject_code'] == "COMP":
        courses.append(Course.from_parsed_data(course_data))

# go through each course, pass it the list of courses already completed, and determine whether the
# course can be taken. If it can be, add it to the happy list of results.
results_list = []
for course in courses:
    if course.check_requirements_satisfied(list_of_courses_completed):
        results_list.append(course)

# step 3 - display the output to the user
# again a GUI would be better.
# it would be nice to display more information about a course than just its name
# a link to the course's page on P&C website, at least

results_list = sorted(results_list, key=lambda x: x.course_code)

print()
print()
print(str(len(results_list)) + " potential elective COMP courses found.")
print()

for course in results_list:
    print(str(course))
__author__ = 'caitelatte'

import argparse
from os import makedirs
import Scraper.CollectionScraper
import Scraper.CourseScraper
import Scraper.ProgramScraper

"""
Caitlin Macleod u5351248
6/10/2015

This script takes arguments when it runs, then calls the three types of scrapers.
usage: python __main__.py -i
"""


def scrape_all(initial, download=True, parse=True, do_courses=True, do_collections=True, do_programs=True):
    """
    Caitlin Macleod u5351248
    7/10/2015

    This function uses ProgramScraper, CollectionScraper and CourseScraper to
    download and parse information about programs, courses and collections from
    ANU's Programs and Courses site.

    :param initial: is this a first-time setup? If True, will set up needed folders and download all information fresh.
                    Defaults to false.
    :param download: should the code download all information again? default = True
    :param parse: should the code parse all information again? default = True
    :param courses: should the code deal with courses? default = True
    :param collections: should the code deal with collections? default = True
    :param programs: should the code deal with programs? default = True
    :return:
    """

    # Creating folders that are used by other processes
    if initial:
        # Assuming being called from project/Scraper.
        folders = ["TempDownloads/Programs", "TempDownloads/Courses", "TempDownloads/Collections",
                   "ParsedData", "Lists"]
        for folder in folders:
            try:
                makedirs(folder)
            except FileExistsError:
                print("Tried to create " + folder + ", but it already exists!")

    # TODO: enable args in CourseScraper.py
    # if args.initial:
    #     out_args = shlex.split("python CourseScraper.py -i")
    # else:
    #     out_args = shlex.split("python CourseScraper.py")
    # course_process = subprocess.Popen(out_args, stdin=subprocess.PIPE)

    if do_collections:
        collections = Scraper.CollectionScraper.scrape_collections(initial=initial, download=download, parse=parse)
    if do_programs:
        programs = Scraper.ProgramScraper.scrape_programs(initial=initial, download=download, parse=parse)
    if do_courses:
        courses = Scraper.CourseScraper.scrape_courses(initial=initial, download=download, parse=parse)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='ANU PandC Scraper and Parser',
        description='Scrape some data about ANU programs and courses and set up some more useful data models for use.')
    parser.add_argument('--initial', '-i', action='store_true', default=False,
                        help='the scraper will do everything, from initialising folders to downloading and parsing information. (default: %(default))')
    parser.add_argument('--download', '-d', action='store_true', default=False,
                        help='the scraper will download everything as it goes (default: %(default))')
    parser.add_argument('--parse', '-p', action='store_true', default=False,
                        help='the scraper will parse the information as it goes (default: %(default))')

    args = parser.parse_args()

    scrape_all(args.initial, download=args.download, parse=args.parse)


Notes

By Nick Sifniotis 29/09/2015

Get JSON list of courses

- [http://programsandcourses.anu.edu.au/data/ProgramSearch/GetPrograms?ShowAll=true&Careers[0]=Undergraduate](http://programsandcourses.anu.edu.au/data/ProgramSearch/GetPrograms?ShowAll=true&Careers[0]=Undergraduate)
- [http://programsandcourses.anu.edu.au/data/MajorSearch/GetMajors?ShowAll=true&Careers[0]=Undergraduate](http://programsandcourses.anu.edu.au/data/MajorSearch/GetMajors?ShowAll=true&Careers[0]=Undergraduate)
- [http://programsandcourses.anu.edu.au/data/MinorSearch/GetMinors?ShowAll=true&Careers[0]=Undergraduate](http://programsandcourses.anu.edu.au/data/MinorSearch/GetMinors?ShowAll=true&Careers[0]=Undergraduate)
- [http://programsandcourses.anu.edu.au/data/SpecialisationSearch/GetSpecialisations?ShowAll=true&Careers[0]=Undergraduate](http://programsandcourses.anu.edu.au/data/SpecialisationSearch/GetSpecialisations?ShowAll=true&Careers[0]=Undergraduate)
- [http://programsandcourses.anu.edu.au/data/CourseSearch/GetCourses?ShowAll=true&Careers[0]=Undergraduate](http://programsandcourses.anu.edu.au/data/CourseSearch/GetCourses?ShowAll=true&Careers[0]=Undergraduate)

Filters
ShowAll=true                    # get them all
Careers[0]=Undergraduate        # only the ugrad programs

Other filters can be inferred from the source code of programsandcourses.anu.edu.au/Search

Nick 23:15 30/09/2015
I think we need to revisit how the html is being parsed. The programs pages contain a lot of references to collections and courses.
Likewise, courses reference collections and programs, and collections reference courses.

It would be helpful if we had a list of program, course, collection names so that the parser would be able to check whether
something that it thinks is a course name or a program name is in fact one - and if so, maybe even pull a reference to the
object that will eventually hold it.

What I'm thinking is a sort of two stage scraping process. The first stage is downloading and processing the JSON from the
links above, and using that data we create dictionaries with the course/program/collection name as the key 
and the object as the value.

The second stage is downloading and parsing the individual files for each program, course and collection. The job would
be made easier by being able to get answers to questions like 'is COMP1040 a course'

This isn't radically different to what I've already done for the programs. It just means that individual parsing
could not occur until dictionaries have been created for everything, not just the programs.

I hope that made sense.

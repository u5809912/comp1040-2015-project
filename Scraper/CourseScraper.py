__author__ = 'nsifniotis'

from DataModel.Course import Course
from Scraper.DataRepo import Repository
import json
import argparse
from urllib.request import urlretrieve

"""
Nick Sifniotis u5809912
28/10/2015

A scraper for courses. Code very very much based on Caitlin's CollectionScraper class.
The JSON data that is being scraped comes from
http://programsandcourses.anu.edu.au/data/CourseSearch/GetCourses?ShowAll=true&Careers[0]=Undergraduate&SelectedYear=<<year>>

"""


def jdefault(o):
    """
    Caitlin Macleod u5351248
    06/10/2015

    The idea of setting a different default for exporting objects to json came from:
     http://pythontips.com/2013/08/08/storing-and-loading-data-with-json/

    :param o: object to be encoded
    :return:
    """
    return o.__dict__


def create_file(filename):
    """
    Caitlin Macleod u5351248

    Utility function to create or empty a file just before using it.

    :param filename: the path and name of the file in question.
    :return: nil
    """
    with open(filename, "w+") as f:
        pass


def scrape_courses(initial=True, download=True, parse=True):
    """
    Caitlin Macleod u5351248 & Nick Sifniotis u5809912
    Last modified 27/10/2015

    This contains
    :param initial: boolean denoting if this is the initial scrape. Default=True
    :param download: boolean denoting if the scraper should download html files for each program. Default=True
    :param parse: boolean denoting if the scraper should parse each programs's html files. Default=True
    :return: dictionary of program objects
    """
    courses = {}

    # If the program was asked to initialise or download everything, go through and download everything again.
    if initial or download:
        print("Scraping Courses")
        filename = "TempDownloads/TempCourses.json"
        if initial or download:
            url = "http://programsandcourses.anu.edu.au/data/CourseSearch/GetCourses?ShowAll=true&Careers[0]=Undergraduate"
            create_file(filename)
            source, headers = urlretrieve(url, filename)
        else:
            source = filename
        json_data = open(source)
        course_data = json.load(json_data)["Items"]

        # Initialise object and add it to programs with a key of its code
        for course_item in course_data:
            course = Course.from_search_item(course_item)
            courses[course.course_code] = course

        # Downloading html files for every program
        total_courses = len(courses)
        course_counter = 1
        if initial or download:
            for course_code, course in courses.items():
                print("Downloading " + course_code + " " + str(course_counter) + "/" + str(total_courses), end="")
                course.download_course_data()
                course_counter += 1

        # Dump basic info in a json in Scraper/Lists (overwriting anything that was there before.
        print("Dumping basic Course info in Scraper/Lists/Courses.json")
        with open('Lists/Courses.json', 'w') as dumpfile:
            json.dump(courses, dumpfile, default=jdefault, sort_keys=True, indent=4, ensure_ascii=False)
    else:
        # If not asked to download or initialise, build each object again from Lists/Programs.json,
        # check if TempDownloads/Programs/ProgramCode.html is there, and if not, download it.
        with open('Lists/Courses.json') as list_file:
            list_data = json.load(list_file)
        for course_code, course_data in list_data.items():
            courses[course_code] = Course.from_json_list(course_data)

    # Parse data
    if parse:
        repo = Repository("")
        for course_code, course in courses.items():
            course.parse_data(repo)

    # todo: save parsed collections in Scraper/ParsedData.

    return courses


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='ANU PandC Courses Scraper and Parser',
        description='Scrape some data about ANU courses and set up some more useful data models for use.')
    parser.add_argument('--initial', '-i', action='store_true', default=False,
                        help='the scraper will download everything as it goes (default: %(default))')

    args = parser.parse_args()

    scrape_courses(args.initial)

__author__ = 'nsifniotis'

import json
import os
from DataModel.Course import Course
from DataModel.Collection import Collection
from DataModel.Program import Program

class ANU:

    def __init__(self):
        self.colleges = dict()
        self.colleges['the Australian National University'] = 'ANU'
        self.colleges['Australian National University'] = 'ANU'
        self.colleges['College of Arts and Social Sciences'] = 'CASS'
        self.colleges['College of Asia and the Pacific'] = 'CAP'
        self.colleges['College of Business and Economics'] = 'CBE'
        self.colleges['College of Engineering and Computer Science'] = 'CECS'
        self.colleges['College of Law'] = 'CL'
        self.colleges['College of Medicine, Biology and Environment'] = 'CMBE'
        self.colleges['College of Physical and Mathematical Sciences'] = 'CPMS'

    def filter_item(self, item):
        for key in self.colleges:
            item = item.replace(key, self.colleges[key])

        return item


class Repository:
    """
    Nick Sifniotis u5809912
    27/10/2015

    This repository class contains the lists of programs, courses and collections that the parser will need
    to make sense of the data on the P&C website.

    All these items are indexed by both name and code because you just never know what the bloody ANU website
    will throw at you. The key function in this class is identify_me, which will accept a string and attempt
    to ascertain whether that string refers to a program, a course, a collection, or none of the above.
    """
    def __init__(self, path):
        # collections data
        # load the JSON file that contains the collections
        source = os.path.join(path, "Lists/Collections.json")
        json_data = open(source)
        collections = json.load(json_data)

        self.majors_name = dict()
        self.majors_code = dict()

        self.minors_name = dict()
        self.minors_code = dict()

        self.specialisations_name = dict()
        self.specialisations_code = dict()

        for item in collections.values():
            item = Collection.fromlistjson(item)

            if item.type == "MAJ":
                self.majors_name[item.name] = item
                self.majors_code[item.code] = item
            elif item.type == "MIN":
                self.minors_name[item.name] = item
                self.minors_code[item.code] = item
            elif item.type == "SPC":
                self.specialisations_name[item.name] = item
                self.specialisations_code[item.code] = item

        # programs data
        # load the JSON file that contains the list of programs
        source = os.path.join(path, "Lists/Programs.json")
        json_data = open(source)
        program_list = json.load(json_data)

        self.programs_by_code = dict()
        self.programs_by_name = dict()
        for program in program_list.values():
            program = Program.from_json_list(program)
            self.programs_by_code[program.program_code] = program
            self.programs_by_name[program.program_name] = program

        #courses data
        # load the JSON file that contains the list of courses
        source = os.path.join(path, "Lists/Courses.json")
        json_data = open(source)
        course_list = json.load(json_data)

        self.courses_by_code = dict()
        self.courses_by_name = dict()
        for course in course_list.values():
            course = Course.from_json_list(course)
            self.courses_by_code[course.course_code] = course
            self.courses_by_name[course.course_name] = course

        # ANU data
        self.ANU = ANU()

    def get_me(self, mystery_string, hint=""):
        """
        Nick Sifniotis u5809912
        06/11/2015

        Wrapper method for identify_me
        Returns the item, if found.
        Not interested in the sort of item that the thing is

        :param mystery_string: I don't know what this is. It could be a program, a college name, or nothing at all ..
        :param hint: .. but I think it might be this sort of thing
        :return: the found thing, or None
        """
        item_type, item = self.identify_me(mystery_string, hint)
        return item

    def identify_me(self, mystery_string, hint=""):
        """
        Nick Sifniotis u5809912
        27/10/2015

        This is one of the most important methods in the data scraper.

        Given a mystery string, aptly called 'mystery_string', identify_me searches through this
        repository's dictionaries of data to try and ascertain what the mystery string is referring to.

        If mystery_string is found, then return the type of object that it refers to [eventually it will be
        an enumeration, assuming Python supports enumerations, but for now it will be an encoded string:
        "PROG", "CRS", "MAJ", "MIN", "SPC", "COL" or "ANU"] and a reference to the object itself.

        This is the method that the parser will use when it is trying to translate HTML into actual lists of courses.

        hint is an optional parameter that suggests to this method where it should begin its search. Oftentimes
        the parser will know what sort of object to expect, and it might save time (and potentially resolve
        ambiguous names!) to begin the search there.

        :param mystery_string: I don't know what this is. It could be a program, a college name, or nothing at all ..
        :param hint: .. but I think it might be this sort of thing
        :return: a tuple containing the sort of thing that this is, and the thing itself
                 (because they don't all inherit from the same base class. Maybe they should ...)
        """
        if hint != "":
            thing = self.__am_i_this_thing(mystery_string, hint)
            if thing is not None:
                return hint, thing

        types_of_things = ["PROG", "CRS", "MAJ", "MIN", "SPC", "COL", "ANU"]
        for thing_type in types_of_things:
            thing = self.__am_i_this_thing(mystery_string, thing_type)
            if thing is not None:
                return thing_type, thing

        return None, None

    def __am_i_this_thing(self, mystery, type_of_thing):
        """
        Nick Sifniotis u5809912
        27/10/2015

        The private method that does the grunt work for identify_me above.

        Checks to see whether the string mystery is a key that connects to an object
        of type type_of_thing.

        :param mystery: the string that is being identified
        :param type_of_thing: the sort of thing that we think the string is
        :return: if the string is indeed a thing, return that thing. Else return None
        """
        list1 = None
        list2 = None

        if type_of_thing == "PROG":
            list1 = self.programs_by_name
            list2 = self.programs_by_code
        elif type_of_thing == "CRS":
            list1 = self.courses_by_name
            list2 = self.courses_by_code
        elif type_of_thing == "MAJ":
            list1 = self.majors_name
            list2 = self.majors_code
        elif type_of_thing == "MIN":
            list1 = self.minors_name
            list2 = self.minors_code
        elif type_of_thing == "SPC":
            list1 = self.specialisations_name
            list2 = self.specialisations_code
        # elif type_of_thing == "COL":
        # elif type_of_thing == "ANU":

        if list1 is None or list2 is None:
            return None

        if mystery in list1:
            return list1[mystery]
        if mystery in list2:
            return list2[mystery]

        return None

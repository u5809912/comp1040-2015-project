__author__ = 'nsifniotis'


class Tokeniser:

    def __init__(self, input_list):
        self.data = input_list
        self.position = 0
        self.num_data = len(input_list)

    def get(self):
        """
        Nick Sifniotis u5809912
        06/10/2015

        Advance the tokeniser and return the next item
        :return:
        """
        return None if self.position >= self.num_data else self.data[self.position]

    def next(self):
        self.position += 1
        while self.position < self.num_data and self.data[self.position] == "":
            self.position += 1

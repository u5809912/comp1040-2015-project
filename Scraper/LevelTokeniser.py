__author__ = 'nsifniotis'


class LevelTokeniser:
    """
    Nick Sifniotis u5809912
    30/09/2015

    A quick and dirty 'tokeniser' to facilitate the creation of a program's
    'tree' of conditions.

    Accepts the raw data from the first stage of parsing and feeds it to the
    tree builder line by line.

    """

    def __init__(self, dataset):
        self.data_set = dataset
        self.curr_position = -1
        self.list_length = len(dataset)


    def eof(self):
        """
        Nick Sifniotis u5809912
        30/09/2015

        :return: true if the end of the data set has been reached, false otherwise
        """
        return (self.curr_position == (self.list_length - 1))


    def advance(self):
        """
        Nick Sifniotis u5809912
        30/09/2015

        Advance the tokeniser to the next token.

        :return:
        """
        self.curr_position += 1


    def CurrentItem(self):
        """
        Nick Sifniotis u5809912
        30/09/2015

        :return: the current item
        """
        (item, level) = self.data_set[self.curr_position]
        return item


    def CurrentLevel(self):
        """
        Nick Sifniotis u5809912
        30/09/2015

        :return: the current item's depth
        """
        (item, level) = self.data_set[self.curr_position]
        return level


    def NextLevel(self):
        """
        Nick Sifniotis u5809912
        30/09/2015

        :return: the depth of the next item, or, if there is no next item, zero
                 to force the recursive tree builder calls to finish cleanly
        """
        if self.eof():
            return 0
        else:
            (item, level) = self.data_set[self.curr_position + 1]
            return level
__author__ = 'nsifniotis'

from DataModel.Program import Program
from Scraper.DataRepo import Repository
import json
import argparse
from urllib.request import urlretrieve

"""
Nick Sifniotis u5809912
27/10/2015

A scraper for programs. Code very very much based on Caitlin's CollectionScraper class.
The JSON data that is being scraped comes from
http://programsandcourses.anu.edu.au/data/ProgramSearch/GetPrograms?ShowAll=true&Careers[0]=Undergraduate

"""


def jdefault(o):
    """
    Caitlin Macleod u5351248
    06/10/2015

    The idea of setting a different default for exporting objects to json came from:
     http://pythontips.com/2013/08/08/storing-and-loading-data-with-json/

    :param o: object to be encoded
    :return:
    """
    return o.__dict__


def create_file(filename):
    """
    Caitlin Macleod u5351248

    Utility function to create or empty a file just before using it.

    :param filename: the path and name of the file in question.
    :return: nil
    """
    with open(filename, "w+") as f:
        pass


def scrape_programs(initial=True, download=True, parse=True):
    """
    Caitlin Macleod u5351248 & Nick Sifniotis u5809912
    Last modified 27/10/2015

    This contains
    :param initial: boolean denoting if this is the initial scrape. Default=True
    :param download: boolean denoting if the scraper should download html files for each program. Default=True
    :param parse: boolean denoting if the scraper should parse each programs's html files. Default=True
    :return: dictionary of program objects
    """
    programs = {}

    # If the program was asked to initialise or download everything, go through and download everything again.
    if initial or download:
        print("Scraping Programs")
        filename = "TempDownloads/TempPrograms.json"
        if initial or download:
            url = "http://programsandcourses.anu.edu.au/data/ProgramSearch/GetPrograms?ShowAll=true&Careers[0]=Undergraduate"
            create_file(filename)
            source, headers = urlretrieve(url, filename)
        else:
            source = filename
        json_data = open(source)
        program_data = json.load(json_data)["Items"]

        # Initialise object and add it to programs with a key of its code
        for program_item in program_data:
            program = Program.from_search_item(program_item)
            programs[program.program_code] = program

        # Downloading html files for every program
        total_programs = len(programs)
        program_counter = 1
        if initial or download:
            for program_code, program in programs.items():
                print("Downloading " + program_code + " " + str(program_counter) + "/" + str(total_programs), end="")
                program.download_data()
                program_counter += 1

        # Dump basic info in a json in Scraper/Lists (overwriting anything that was there before.
        print("Dumping basic Program info in Scraper/Lists/Programs.json")
        with open('Lists/Programs.json', 'w') as dumpfile:
            json.dump(programs, dumpfile, default=jdefault, sort_keys=True, indent=4, ensure_ascii=False)
    else:
        # If not asked to download or initialise, build each object again from Lists/Programs.json,
        # check if TempDownloads/Programs/ProgramCode.html is there, and if not, download it.
        with open('Lists/Programs.json') as list_file:
            list_data = json.load(list_file)
        for program_code, program_data in list_data.items():
            programs[program_code] = Program.from_json_list(program_data)

    # Parse data
    if parse:
        repo = Repository("")
        for program_code, program in programs.items():
            program.parse_data(repo)

    # todo: save parsed collections in Scraper/ParsedData.

    return programs


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='ANU PandC Programs Scraper and Parser',
        description='Scrape some data about ANU programs and set up some more useful data models for use.')
    parser.add_argument('--initial', '-i', action='store_true', default=False,
                        help='the scraper will download everything as it goes (default: %(default))')

    args = parser.parse_args()

    scrape_programs(args.initial)

__author__ = 'caitelatte'

import argparse
from Scraper.DataRepo import Repository
from DataModel.Collection import Collection
from urllib.request import urlretrieve
import json

"""
Caitlin Macleod u5351248
06/10/2015

A scraper for collections (Majors, Minors and Specialisations). Has to go through three different json files.
http://programsandcourses.anu.edu.au/data/MajorSearch/GetMajors?ShowAll=true&Careers[0]=Undergraduate
http://programsandcourses.anu.edu.au/data/MinorSearch/GetMinors?ShowAll=true&Careers[0]=Undergraduate
http://programsandcourses.anu.edu.au/data/SpecialisationSearch/GetSpecialisations?ShowAll=true&Careers[0]=Undergraduate
"""


def jdefault(o):
    """
    Caitlin Macleod u5351248
    06/10/2015

    The idea of setting a different default for exporting objects to json came from:
     http://pythontips.com/2013/08/08/storing-and-loading-data-with-json/

    :param o: object to be encoded
    :return:
    """
    return o.__dict__


def create_file(filename):
    """
    Caitlin Macleod u5351248

    Utility function to create or empty a file just before using it.

    :param filename: the path and name of the file in question.
    :return: nil
    """
    with open(filename, "w+") as f:
        pass


def scrape_collections(initial=True, download=True, parse=True):
    """
    Caitlin Macleod u5351248
    7/10/2015

    This contains
    :param initial: boolean denoting if this is the initial scrape. Default=True
    :param download: boolean denoting if the scraper should download html files for each collection. Default=True
    :param parse: boolean denoting if the scraper should parse each collection's html files. Default=True
    :return: dictionary of collection objects
    """
    collections = {}

    # If the program was asked to initialise or download everything, go through and download everything again.
    if initial or download:
        # Scraping each Major, Minor and Specialisation, taking advantage of consistency in URLs and filenames.
        types_of_collections = ["Major", "Minor", "Specialisation"]
        for coll_type in types_of_collections:
            print("Scraping "+coll_type+"s")
            filename = "TempDownloads/Temp"+coll_type+"s.json"
            if initial or download:
                url = "http://programsandcourses.anu.edu.au/data/"+coll_type+"Search/Get"+coll_type+"s?ShowAll=true&Careers[0]=Undergraduate"
                create_file(filename)
                source, headers = urlretrieve(url, filename)
            else:
                source = filename
            json_data = open(source)
            coll_data = json.load(json_data)["Items"]

            # Initialise collection object and add it to collections with a key of its code
            for coll_item in coll_data:
                coll = Collection.fromsearchitem(coll_item)
                collections[coll.code] = coll

        # Downloading html files for every collection
        total_coll = len(collections)
        coll_counter = 1
        if initial or download:
            for collcode, collection in collections.items():
                print("Downloading " + collcode + " " + str(coll_counter) + "/" + str(total_coll), end="")
                collection.download_data()
                coll_counter += 1

        # Dump basic info in a json in Scraper/Lists (overwriting anything that was there before.
        print("Dumping basic Collection info in Scraper/Lists/Collection.json")
        with open('Lists/Collections.json', 'w') as dumpfile:
            json.dump(collections, dumpfile, default=jdefault, sort_keys=True, indent=4, ensure_ascii=False)
    else:
        # If not asked to download or initialise, build each object again from Lists/Collections.json,
        # check if TempDownloads/Collections/CourseCode.html is there, and if not, download it.
        with open('Lists/Collections.json') as list_file:
            list_data = json.load(list_file)
        for coll_code, coll_data in list_data.items():
            collections[coll_code] = Collection.fromlistjson(coll_data)

    # Parse data
    if parse:
        repo = Repository("")
        for collcode, collection in collections.items():
            collection.parse_data(repo)

        # Dump complete info in a json in Scraper/ParsedData (overwriting anything that was there before)
        print("Dumping complete Collection info in Scraper/ParsedData/Collection.json")
        with open('ParsedData/Collections.json', 'w') as dumpfile:
            json.dump(collections, dumpfile, default=jdefault, sort_keys=True, indent=4, ensure_ascii=False)

    return collections


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='ANU PandC Collections Scraper and Parser',
        description='Scrape some data about ANU collections and set up some more useful data models for use.')
    parser.add_argument('--initial', '-i', action='store_true', default=False,
                        help='the scraper will download everything as it goes (default: %(default))')

    args = parser.parse_args()

    scrape_collections(args.initial)


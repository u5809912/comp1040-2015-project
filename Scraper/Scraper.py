__author__ = 'nsifniotis'

from urllib.request import urlretrieve
from DataModel.Collection import *
from DataModel.Course import *
from DataModel.Program import *
from SystemState import *
from GUI.Utils import *
import json


def download_lists(viewport, data_set):

    # hard code these URLs for the moment..
    programs = ["http://programsandcourses.anu.edu.au/data/ProgramSearch/GetPrograms?ShowAll=true&Careers[0]=Undergraduate&SelectedYear="]
    courses = ["http://programsandcourses.anu.edu.au/data/CourseSearch/GetCourses?ShowAll=true&Careers[0]=Undergraduate&SelectedYear="]
    colls = ["http://programsandcourses.anu.edu.au/data/MajorSearch/GetMajors?ShowAll=true&Careers[0]=Undergraduate&SelectedYear=",
                    "http://programsandcourses.anu.edu.au/data/MinorSearch/GetMinors?ShowAll=true&Careers[0]=Undergraduate&SelectedYear=",
                    "http://programsandcourses.anu.edu.au/data/SpecialisationSearch/GetSpecialisations?ShowAll=true&Careers[0]=Undergraduate&SelectedYear="]

    collections = {}
    for url in colls:
        url = url + data_set
        temp_filename = "temp.json"
        create_file(temp_filename)
        status_message(viewport, "Downloading collections lists.")
        source, headers = urlretrieve(url, temp_filename)

        json_data = open(source)
        coll_data = json.load(json_data)["Items"]

        # Initialise collection object and add it to collections with a key of its code
        for coll_item in coll_data:
            coll = Collection.fromsearchitem(coll_item)
            collections[coll.code] = coll

        os.remove("temp.json")

    # collections now has all the data we need about collections.
    with open(directories['downloads_root'] + work_types['lists'] + data_set + '/Collections.json', 'w') as dumpfile:
        json.dump(collections, dumpfile, default=jdefault, sort_keys=True, indent=4, ensure_ascii=False)

    collections = {}
    for url in programs:
        url = url + data_set
        temp_filename = "temp.json"
        create_file(temp_filename)
        status_message(viewport, "Downloading programs list.")
        source, headers = urlretrieve(url, temp_filename)

        json_data = open(source)
        coll_data = json.load(json_data)["Items"]

        # Initialise collection object and add it to collections with a key of its code
        for coll_item in coll_data:
            coll = Program.from_search_item(coll_item)
            collections[coll.program_code] = coll

        os.remove("temp.json")

    # collections now has all the data we need about programs.
    with open(directories['downloads_root'] + work_types['lists'] + data_set + '/Programs.json', 'w') as dumpfile:
        json.dump(collections, dumpfile, default=jdefault, sort_keys=True, indent=4, ensure_ascii=False)

    collections = {}
    for url in courses:
        url = url + data_set
        temp_filename = "temp.json"
        create_file(temp_filename)
        status_message(viewport, "Downloading course list.")
        source, headers = urlretrieve(url, temp_filename)

        json_data = open(source)
        coll_data = json.load(json_data)["Items"]

        # Initialise collection object and add it to collections with a key of its code
        for coll_item in coll_data:
            coll = Course.from_search_item(coll_item)
            collections[coll.course_code] = coll

        os.remove("temp.json")

    # collections now has all the data we need about programs.
    with open(directories['downloads_root'] + work_types['lists'] + data_set + '/Courses.json', 'w') as dumpfile:
        json.dump(collections, dumpfile, default=jdefault, sort_keys=True, indent=4, ensure_ascii=False)

    alert(viewport, "Finished downloading lists. Press any key to continue.")
    return


def jdefault(o):
    """
    Caitlin Macleod u5351248
    06/10/2015

    The idea of setting a different default for exporting objects to json came from:
     http://pythontips.com/2013/08/08/storing-and-loading-data-with-json/

    :param o: object to be encoded
    :return:
    """
    return o.__dict__
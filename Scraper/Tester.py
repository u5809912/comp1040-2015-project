__author__ = 'nsifniotis'

from Scraper.DataRepo import Repository
from DataModel.Collection import Collection
from DataModel.Course import Course

repo = Repository()

res = []
for course in repo.courses_by_code.values():
    if course.subject_code == "COMP":
        res.append(course)

res = sorted(res, key=lambda c: c.course_code)

for item in res:
    print (item.course_code)

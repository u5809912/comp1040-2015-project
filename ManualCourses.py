__author__ = 'nsifniotis'

"""
Nick Sifniotis u5809912
28/10/2015

Holy shit.

That is all.
"""

from DataModel.CourseList import FiniteList, InfiniteList
from Scraper.DataRepo import Repository
import json


# todo test the new AND and OR classes by converting this manual course info over to use them
# in particular note the COMP2600 | 6 units 2000 level maths condition that hereto has been ignored

def jdefault(o):
    """
    Caitlin Macleod u5351248
    06/10/2015

    The idea of setting a different default for exporting objects to json came from:
     http://pythontips.com/2013/08/08/storing-and-loading-data-with-json/

    :param o: object to be encoded
    :return:
    """
    return o.__dict__

repo = Repository("")

#
# I am going to manually insert the data for a few courses
# until the parser is up and going.
#

# 1030 no prereqs

# 1040
comp1040 = repo.get_me("COMP1040", "CRS")
holding_list = FiniteList(0, "EXACT")
holding_list.add_course("COMP1730", repo)
comp1040.incompatible_courses.append(holding_list)

# 1100 uwe zimmer!
comp1100 = repo.get_me("COMP1100", "CRS")
holding_list = FiniteList(0, "EXACT")
holding_list.add_course("COMP1130", repo)
comp1100.incompatible_courses.append(holding_list)

# 1130 uwe zimmer! But nothing :( surely 1100 would be incompatible?

# 1110 steve blackburn!
comp1110 = repo.get_me("COMP1110", "CRS")
holding_list = FiniteList(0, "EXACT")
holding_list.add_courses(["COMP1140", "COMP1510", "COMP2750"], repo)
comp1110.incompatible_courses.append(holding_list)
holding_list = FiniteList(1, "MIN", True)
holding_list.add_courses(["COMP1100", "COMP1130", "COMP1730"], repo)
comp1110.required_courses.append(holding_list)

# 1140 steve blackburn!
comp1140 = repo.get_me("COMP1140", "CRS")
holding_list = FiniteList(0, "EXACT")
holding_list.add_courses(["COMP1110", "COMP1510"], repo)
comp1140.incompatible_courses.append(holding_list)
holding_list = FiniteList(1, "MIN", True)
holding_list.add_course("COMP1130", repo)
comp1140.required_courses.append(holding_list)


# 1510
comp1510 = repo.get_me("COMP1510", "CRS")
holding_list = FiniteList(0, "EXACT")
holding_list.add_courses(["COMP1110", "COMP1140", "COMP2750"], repo)
comp1510.incompatible_courses.append(holding_list)
holding_list = FiniteList(1, "MIN", True)
holding_list.add_course(["COMP1100", "COMP1130"], repo)
comp1510.required_courses.append(holding_list)

# 1710 - nothing

# 1720 - nothing

# 1730
comp1730 = repo.get_me("COMP1730", "CRS")
holding_list = FiniteList(0, "EXACT")
holding_list.add_course("COMP6730", repo)
comp1730.incompatible_courses.append(holding_list)

# 2100
comp2100 = repo.get_me("COMP2100", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP1110", repo)
holding_list.add_course("COMP1140", repo)
holding_list.add_course("COMP1510", repo)
comp2100.required_courses.append(holding_list)
holding_list = InfiniteList(6, "MIN")
holding_list.level = [1000]
holding_list.subject_code = ["MATH"]
comp2100.required_courses.append(holding_list)
holding_list = FiniteList(0, "EXACT")
holding_list.add_course("COMP2500", repo)
comp2100.incompatible_courses.append(holding_list)

# 2130
comp2130 = repo.get_me("COMP2130", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP2100", repo)
holding_list.add_course("COMP2500", repo)
holding_list.add_course("INFS2024", repo)
comp2130.required_courses.append(holding_list)
holding_list = InfiniteList(6, "MIN")
holding_list.level = [1000]
holding_list.subject_code = ["MATH"]
comp2130.required_courses.append(holding_list)
holding_list = FiniteList(0, "EXACT")
holding_list.add_course("COMP3110", repo)
holding_list.add_course("COMP6311", repo)
comp2130.incompatible_courses.append(holding_list)

# 2140
comp2140 = repo.get_me("COMP2140", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP1040", repo)
comp2140.required_courses.append(holding_list)
holding_list = FiniteList(0, "EXACT")
holding_list.add_course("COMP1110", repo)
holding_list.add_course("COMP1140", repo)
holding_list.add_course("COMP1510", repo)
comp2140.incompatible_courses.append(holding_list)

# 2300
comp2300 = repo.get_me("COMP2300", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP1100", repo)
holding_list.add_course("COMP1130", repo)
holding_list.add_course("COMP1710", repo)
comp2300.required_courses.append(holding_list)
holding_list = InfiniteList(6, "MIN")
holding_list.level = [1000]
holding_list.subject_code = ["MATH"]
comp2300.required_courses.append(holding_list)

# 2310
comp2310 = repo.get_me("COMP2310", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP1100", repo)
holding_list.add_course("COMP1130", repo)
holding_list.add_course("COMP1140", repo)
holding_list.add_course("COMP1510", repo)
comp2310.required_courses.append(holding_list)

# 2400
comp2400 = repo.get_me("COMP2400", "CRS")
holding_list = FiniteList(0, "EXACT")
holding_list.add_course("COMP6240", repo)
comp2400.incompatible_courses.append(holding_list)

# 2410
comp2410 = repo.get_me("COMP2410", "CRS")
holding_list = InfiniteList(6, "MIN")
holding_list.subject_code = ["MATH"]
holding_list.level = [1000]
comp2410.required_courses.append(holding_list)
holding_list = InfiniteList(6, "MIN")
holding_list.subject_code = ["COMP"]
holding_list.level = [1000]
comp2410.required_courses.append(holding_list)

# 2500
comp2500 = repo.get_me("COMP2500", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP1510", repo)
holding_list.add_course("COMP1110", repo)
comp2500.required_courses.append(holding_list)
holding_list = InfiniteList(6, "MIN")
holding_list.subject_code = ["MATH"]
holding_list.level = [1000]
comp2500.required_courses.append(holding_list)
holding_list = FiniteList(0, "EXACT")
holding_list.add_course("COMP2100", repo)
comp2500.incompatible_courses.append(holding_list)
comp2500.special_conditions.append("You must be enrolled in the Bachelor of Software Engineering to take this course.")

# 2550
comp2550 = repo.get_me("COMP2550", "CRS")
comp2550.special_conditions.append("You must be enrolled in the Bachelor of Advanced Computing to take this course.")

# 2560
comp2560 = repo.get_me("COMP2560", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP2550", repo)
comp2560.required_courses.append(holding_list)
comp2560.special_conditions.append("You must be enrolled in the Bachelor of Advanced Computing to take this course.")

# 2600
comp2600 = repo.get_me("COMP2600", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP1110", repo)
holding_list.add_course("COMP1140", repo)
holding_list.add_course("COMP1510", repo)
comp2600.required_courses.append(holding_list)
holding_list = FiniteList(6, "MIN")
holding_list.add_course("MATH1005", repo)
holding_list.add_course("MATH1014", repo)
holding_list.add_course("MATH1116", repo)
comp2600.required_courses.append(holding_list)

# 2610 - nothing ?!

# 2620
comp2620 = repo.get_me("COMP2620", "CRS")
holding_list = InfiniteList(12, "MIN")
holding_list.subject_code = ["COMP", "MATH"]
comp2620.required_courses.append(holding_list)


# 2710
comp2710 = repo.get_me("COMP2710", "CRS")
comp2710.special_conditions.append("You will need to contact the Research School of Computer Science" +
                                   " to request a permission code to enrol in this course.")


# third year units!!

# COMP3006: Computer Science Research Project
comp3006 = repo.get_me("COMP3006", "CRS")
holding_list = InfiniteList(12, "MIN")
holding_list.level = [3000]
holding_list.subject_code = ["COMP"]
comp3006.required_courses.append(holding_list)
comp3006.special_conditions.append("You must be enrolled in the PhB, BCS(Hons) or BAC programs to enrol in this unit.")

# COMP3100: Software Engineering Group Project
comp3100 = repo.get_me("COMP3100", "CRS")
holding_list = FiniteList(12, "EXACT")
holding_list.add_course("COMP2100", repo)
holding_list.add_course("COMP2130", repo)
comp3100.required_courses.append(holding_list)
holding_list = FiniteList(0, "EXACT")
holding_list.add_course("COMP3500", repo)
comp3100.incompatible_courses.append(holding_list)

# COMP3120: Managing Software Development
comp3120 = repo.get_me("COMP3120", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("INFS2024", repo)
holding_list.add_course("COMP2130", repo)
comp3120.required_courses.append(holding_list)

# COMP3300: Operating Systems Implementation
# todo this implementation is not perfect - it ignores the 'or 2000 level math courses'
comp3300 = repo.get_me("COMP3300", "CRS")
holding_list = FiniteList(18, "EXACT")
holding_list.add_course("COMP2300", repo)
holding_list.add_course("COMP2310", repo)
holding_list.add_course("COMP2600", repo)
comp3300.required_courses.append(holding_list)

# COMP3310: Computer Networks
comp3310 = repo.get_me("COMP3310", "CRS")
holding_list = FiniteList(12, "EXACT")
holding_list.add_course("COMP2300", repo)
holding_list.add_course("COMP2600", repo)
comp3310.required_courses.append(holding_list)
holding_list = InfiniteList(6, "MIN")
holding_list.level = [2000]
holding_list.subject_code = ["COMP", "INFS"]
comp3310.required_courses.append(holding_list)

# COMP3320: High Performance Scientific Computation
comp3320 = repo.get_me("COMP3320", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP2300", repo)
holding_list.add_course("COMP2100", repo)
holding_list.add_course("COMP2500", repo)
comp3320.required_courses.append(holding_list)
holding_list = FiniteList(6, "EXACT")
holding_list.add_course("COMP2600", repo)
comp3320.required_courses.append(holding_list)
holding_list = InfiniteList(6, "MIN")
holding_list.level = [2000]
holding_list.subject_code = ["COMP"]
comp3320.required_courses.append(holding_list)

# COMP3420: Advanced Databases and Data Mining
comp3420 = repo.get_me("COMP3420", "CRS")
holding_list = FiniteList(6, "EXACT")
holding_list.add_course("COMP2400", repo)
comp3420.required_courses.append(holding_list)
holding_list = InfiniteList(6, "MIN")
holding_list.level = [1000]
holding_list.subject_code = ["COMP"]
comp3420.required_courses.append(holding_list)
holding_list = InfiniteList(6, "MIN")
holding_list.level = [2000]
holding_list.subject_code = ["COMP"]
comp3420.required_courses.append(holding_list)
holding_list = InfiniteList(6, "MIN")
holding_list.level = [1000]
holding_list.subject_code = ["MATH", "STAT"]
comp3420.required_courses.append(holding_list)

# COMP3500: Software Engineering Project
comp3500 = repo.get_me("COMP3500", "CRS")
holding_list = FiniteList(12, "EXACT")
holding_list.add_course("COMP2500", repo)
holding_list.add_course("COMP2130", repo)
comp3500.required_courses.append(holding_list)
holding_list = FiniteList(0, "EXACT")
holding_list.add_course("COMP3100", repo)
comp3500.incompatible_courses.append(holding_list)

# COMP3530: Systems Engineering for Software Engineers
comp3530 = repo.get_me("COMP3530", "CRS")
holding_list = FiniteList(12, "EXACT")
holding_list.add_course("ENGN1211", repo)
holding_list.add_course("COMP2130", repo)
comp3530.required_courses.append(holding_list)
holding_list = FiniteList(0, "EXACT")
holding_list.add_course("ENGN2225", repo)
comp3530.incompatible_courses.append(holding_list)

# COMP3550: Advanced Computing R&D Project
comp3550 = repo.get_me("COMP3550", "CRS")
holding_list = FiniteList(12, "EXACT")
holding_list.add_course("COMP2130", repo)
holding_list.add_course("COMP2560", repo)
comp3550.required_courses.append(holding_list)
comp3550.special_conditions.append("You must be enrolled in the Bachelor of Advanced Computing (R&D)" +
                                   " to take this course.")

# COMP3560: Advanced Computing R&D Industry Experience
comp3560 = repo.get_me("COMP3560", "CRS")
comp3560.special_conditions.append("You must be enrolled in the Bachelor of Advanced Computing (R&D)" +
                                   " to take this course.")

# COMP3600: Algorithms
comp3600 = repo.get_me("COMP3600", "CRS")
holding_list = FiniteList(24, "EXACT")
holding_list.add_course("COMP1110", repo)
holding_list.add_course("COMP1140", repo)
holding_list.add_course("COMP1510", repo)
holding_list.add_course("COMP2600", repo)
comp3600.required_courses.append(holding_list)

# COMP3610: Principles of Programming Languages
comp3610 = repo.get_me("COMP3610", "CRS")
holding_list = FiniteList(12, "EXACT")
holding_list.add_course("COMP2300", repo)
holding_list.add_course("COMP2600", repo)
comp3610.required_courses.append(holding_list)

# COMP3620: Artificial Intelligence
comp3620 = repo.get_me("COMP3620", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP1100", repo)
holding_list.add_course("COMP1130", repo)
comp3620.required_courses.append(holding_list)
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP1110", repo)
holding_list.add_course("COMP1140", repo)
holding_list.add_course("COMP1510", repo)
comp3620.required_courses.append(holding_list)
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP2600", repo)
holding_list.add_course("COMP2620", repo)
comp3620.required_courses.append(holding_list)

# COMP3630: Theory of Computation
comp3630 = repo.get_me("COMP3630", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP2600", repo)
holding_list.add_course("COMP1140", repo)
comp3630.required_courses.append(holding_list)
comp3630.special_conditions.append("Must have completed COMP1140 or COMP2600 with at least a Distinction.")

# COMP3650: System Architectural Understanding and the Human Brain
# todo this is also not a perfect implementation of
# 12 units of 2000 level COMP courses or 12 units of 2000 level PSYC courses.
comp3650 = repo.get_me("COMP3650", "CRS")
holding_list = InfiniteList(12, "MIN")
holding_list.level = [2000]
holding_list.subject_code = ["COMP", "PSYC"]
comp3650.required_courses.append(holding_list)

# COMP3710: Topics in Computer Science
comp3710 = repo.get_me("COMP3710", "CRS")
comp3710.special_conditions.append("You will need to contact the Research School of Computer Science" +
                                   " to request a permission code to enrol in this course.")

# COMP3740: Project Work in Computing
comp3740 = repo.get_me("COMP3740", "CRS")
comp3740.special_conditions.append("You will need to contact the Research School of Computer Science" +
                                   " to request a permission code to enrol in this course.")

# COMP3820: Software Engineering Internship
comp3820 = repo.get_me("COMP3820", "CRS")
comp3820.special_conditions.append("You will need to contact the Research School of Computer Science" +
                                   " to request a permission code to enrol in this course.")

# COMP3900: Human Computer Interface Design and Evaluation
comp3900 = repo.get_me("COMP3900", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP1110", repo)
holding_list.add_course("COMP1140", repo)
holding_list.add_course("COMP1510", repo)
comp3900.required_courses.append(holding_list)
holding_list = InfiniteList(12, "MIN")
holding_list.level = [2000]
holding_list.subject_code = ["COMP"]
comp3900.required_courses.append(holding_list)


# 4th year units!!!!!
# COMP4005F: Computer Science IV Honours
comp4005f = repo.get_me("COMP4005F", "CRS")
comp4005f.special_conditions.append("You will need to contact the Research School of Computer Science" +
                                    " to request a permission code to enrol in this course.")

# COMP4005P: Computer Science IV Honours
comp4005p = repo.get_me("COMP4005P", "CRS")
comp4005p.special_conditions.append("You will need to contact the Research School of Computer Science" +
                                    " to request a permission code to enrol in this course.")

# COMP4006: Computer Science Honours
comp4006 = repo.get_me("COMP4006", "CRS")
comp4006.special_conditions.append("To enrol in this course you must be studying a BCS (Hns)")
comp4006.special_conditions.append("You will need to contact the Research School of Computer Science" +
                                   " to request a permission code to enrol in this course.")

# COMP4130: Managing Software Quality and Process
comp4130 = repo.get_me("COMP4130", "CRS")
holding_list = FiniteList(12, "EXACT")
holding_list.add_course("COMP3120", repo)
holding_list.add_course("COMP2600", repo)
comp4130.required_courses.append(holding_list)
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP3100", repo)
holding_list.add_course("COMP3500", repo)
comp4130.required_courses.append(holding_list)
comp4130.special_conditions.append("To enrol in this course you must be studying a Bachelor of Software Engineering")

# COMP4300: Parallel Systems
comp4300 = repo.get_me("COMP4300", "CRS")
holding_list = FiniteList(12, "EXACT")
holding_list.add_course("COMP2310", repo)
holding_list.add_course("COMP2600", repo)
comp4300.required_courses.append(holding_list)

# COMP4330: Real-Time & Embedded Systems
comp4330 = repo.get_me("COMP4330", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP2310", repo)
holding_list.add_course("ENGN3213", repo)
comp4330.required_courses.append(holding_list)


# COMP4340: Multicore Computing: Principles and Practice
comp4340 = repo.get_me("COMP4340", "CRS")
holding_list = FiniteList(12, "EXACT")
holding_list.add_course("COMP2310", repo)
holding_list.add_course("COMP2300", repo)
comp4340.required_courses.append(holding_list)
holding_list = InfiniteList(12, "MIN")
holding_list.level = [3000]
holding_list.subject_code = ["COMP"]
comp4340.required_courses.append(holding_list)

# COMP4500: Software Engineering Practice
comp4500 = repo.get_me("COMP4500", "CRS")
holding_list = FiniteList(18, "EXACT")
holding_list.add_course("COMP2130", repo)
holding_list.add_course("COMP3120", repo)
holding_list.add_course("COMP3500", repo)
comp4500.required_courses.append(holding_list)
holding_list = FiniteList(0, "EXACT")
holding_list.add_course("COMP4540", repo)
comp4500.incompatible_courses.append(holding_list)

# COMP4540: Software Engineering Research Project
comp4540 = repo.get_me("COMP4540", "CRS")
comp4540.special_conditions.append("You will need to contact the Research School of Computer Science" +
                                   " to request a permission code to enrol in this course.")

# COMP4550: Advanced Computing Research Project
comp4550 = repo.get_me("COMP4550", "CRS")
comp4550.special_conditions.append("You will need to contact the Research School of Computer Science" +
                                   " to request a permission code to enrol in this course.")

# COMP4560: Advanced Computing Project
comp4560 = repo.get_me("COMP4560", "CRS")
holding_list = FiniteList(0, "EXACT")
holding_list.add_course("COMP4500", repo)
holding_list.add_course("COMP4550", repo)
comp4500.incompatible_courses.append(holding_list)
comp4560.special_conditions.append("To enrol in this course you need to be studying" +
                                   " the Bachelor of Advanced Computing.")

# COMP4600: Advanced Algorithms
comp4600 = repo.get_me("COMP4600", "CRS")
holding_list = FiniteList(6, "EXACT")
holding_list.add_course("COMP3600", repo)
comp4600.required_courses.append(holding_list)
holding_list = InfiniteList(18, "MIN")
holding_list.level = [3000]
holding_list.subject_code = ["COMP"]
comp4600.required_courses.append(holding_list)

# COMP4610: Computer Graphics
comp4610 = repo.get_me("COMP4610", "CRS")
holding_list = InfiniteList(6, "MIN")
holding_list.level = [3000]
holding_list.subject_code = ["COMP"]
comp4610.required_courses.append(holding_list)

# COMP4620: Advanced Topics in Artificial Intelligence
comp4620 = repo.get_me("COMP4620", "CRS")
holding_list = FiniteList(6, "EXACT")
holding_list.add_course("COMP3620", repo)
comp4620.required_courses.append(holding_list)

# COMP4630: Overview of Logic and Computation
comp4630 = repo.get_me("COMP4630", "CRS")
holding_list = InfiniteList(24, "MIN")
holding_list.level = [3000]
holding_list.subject_code = ["COMP"]
comp4630.required_courses.append(holding_list)

# COMP4650: Document Analysis
comp4650 = repo.get_me("COMP4650", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP3410", repo)
holding_list.add_course("COMP3420", repo)
comp4650.required_courses.append(holding_list)
holding_list = FiniteList(6, "EXACT")
holding_list.add_course("COMP2600", repo)
comp4650.required_courses.append(holding_list)
holding_list = InfiniteList(12, "MIN")
holding_list.level = [3000]
holding_list.subject_code = ["COMP", "INFS"]
comp4650.required_courses.append(holding_list)

# COMP4660: Bio-inspired Computing: Applications and Interfaces
comp4660 = repo.get_me("COMP4660", "CRS")
holding_list = InfiniteList(12, "MIN")
holding_list.level = [3000]
holding_list.subject_code = ["COMP"]
comp4660.required_courses.append(holding_list)

# COMP4670: Introduction to Statistical Machine Learning
comp4670 = repo.get_me("COMP4670", "CRS")
holding_list = FiniteList(6, "MIN")
holding_list.add_course("COMP1100", repo)
holding_list.add_course("COMP1130", repo)
holding_list.add_course("COMP1730", repo)
comp4670.required_courses.append(holding_list)
holding_list = InfiniteList(6, "MIN")
holding_list.level = [1000]
holding_list.subject_code = ["MATH"]
comp4650.required_courses.append(holding_list)
holding_list = InfiniteList(12, "MIN")
holding_list.level = [3000]
holding_list.subject_code = ["COMP"]
comp4650.required_courses.append(holding_list)
comp4670.special_conditions.append("You will need to contact the Research School of Computer Science" +
                                   " to request a permission code to enrol in this course.")

# COMP4680: Advanced Topics in Statistical Machine Learning
comp4680 = repo.get_me("COMP4680", "CRS")
holding_list = FiniteList(6, "EXACT")
holding_list.add_course("COMP4670", repo)
comp4680.required_courses.append(holding_list)

# COMP4800: Industrial Experience
comp4800 = repo.get_me("COMP4800", "CRS")
holding_list = FiniteList(6, "EXACT")
holding_list.add_course("COMP3500", repo)
comp4800.required_courses.append(holding_list)

# COMP5920: Exchange Program in Computer Science
comp5920 = repo.get_me("COMP5920", "CRS")
comp5920.special_conditions.append("You will need to contact the Research School of Computer Science" +
                                   " to request a permission code to enrol in this course.")

# force save this data.
# Dump complete info in a json in Scraper/ParsedData (overwriting anything that was there before)
print("Dumping complete Courses info in Scraper/ParsedData/Courses.json")
with open('ParsedData/Courses.json', 'w') as dumpfile:
    json.dump(repo.courses_by_code, dumpfile, default=jdefault, sort_keys=True, indent=4, ensure_ascii=False)

__author__ = 'nsifniotis'

from GUI.Utils import *
from GUI.MenuComponents import *
from SystemState import *
from Scraper import Scraper


datasets = []
current_dataset = -1
viewport = None


def initialise():
    global current_dataset
    global datasets
    global viewport

    # ncurses stuff first
    main_window = curses.initscr()
    curses.start_color()
    curses.noecho()
    curses.curs_set(0)
    main_window.keypad(1)
    curses.raw()
    curses.nonl()

    viewport = Viewport(main_window)

    # program stuff second
    if not check_file("datasets.txt"):
        text_file = open("datasets.txt", "w")
        text_file.write("")
        text_file.close()

    datasets = [line.strip() for line in open('datasets.txt')]
    current_dataset = -1

    # colour pair initialisation third.
    curses.init_pair(viewport.DEFAULT, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    curses.init_pair(viewport.HIGHLIGHT, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.init_pair(viewport.ALERT, curses.COLOR_YELLOW, curses.COLOR_RED)
    curses.init_pair(viewport.STATUS_NOK, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(viewport.STATUS_OK, curses.COLOR_GREEN, curses.COLOR_BLACK)


def main_loop(dimensions):
    global current_dataset

    menu = Menu(dimensions, viewport)
    menu.add_menu_item(MenuItem("Check Directory Status", 1))
    menu.add_menu_item(MenuItem("Select Dataset", 2))
    menu.add_menu_item(MenuItem("Download Lists", 4))
    menu.add_menu_item(MenuItem("Wipe Everything, Start Over", 3))
    menu.add_menu_item(MenuItem("Quit", 0))
    menu.set_title("Main Menu")

    quitting = False            # no big quitters here
    while not quitting:
        next_job = menu.get_menu_choice()
        if next_job == 1:
            results = check_directories(datasets)
            subwind = Window(viewport)
            subwind.set_dimensions(WindowRectangle(40, 1, 55, 20))
            subwind.set_title("Directory Status")
            subwind.set_border_style(WindowBorder.SINGLE)
            subwind.refresh()
            counter = 2
            need_fixing = False
            for (name, status) in results:
                viewport.window.addstr(counter, 41, name)
                if status:
                    viewport.window.addstr(counter, 93, "OK", curses.color_pair(viewport.STATUS_OK))
                else:
                    viewport.window.addstr(counter, 93, "XX", curses.color_pair(viewport.STATUS_NOK))
                    need_fixing = True

                counter += 1

            if need_fixing:
                c = alert(viewport, "Press Y to fix broken directories, or any other key to go back.")
                if c == ord('y') or c == ord('Y'):
                    make_directories(results)
            else:
                alert(viewport, "Directory structure intact. Press any key to go back.")

            viewport.window.clear()

        elif next_job == 2:
            select_dataset()

        elif next_job == 3:
            # wipe everything, oh no!
            hard_directory_reset(datasets)
            current_dataset = -1

        elif next_job == 4:
            download_lists()

        elif next_job == 0:
            quitting = True         # sad


def download_lists():
    if current_dataset < 0 or current_dataset >= len(datasets):
        alert(viewport, "ERROR: No dataset selected.")
        return

    dataset = datasets[current_dataset]
    Scraper.download_lists(viewport, dataset)


def select_dataset():
    """
    Select a dataset to work with, or create a new one.
    :return:
    """
    global current_dataset

    menu = Menu(WindowRectangle(1, 11, 30, 10), viewport)
    menu.set_title("Select Dataset")

    for i in range(0, len(datasets)):
        menu.add_menu_item(MenuItem(datasets[i], i))

    menu.add_menu_item(MenuItem("Add New ..", -1))
    menu.add_menu_item(MenuItem("Go Back", -2))

    item = menu.get_menu_choice()
    if item == -1:
        viewport.window.addstr(30, 2, "Please enter the year to download data for: ")
        viewport.window.refresh()
        curses.echo()
        new_dataset = viewport.window.getstr().decode("utf-8")
        curses.noecho()
        create_new_dataset(new_dataset, datasets)
        alert(viewport.window, "Created dataset " + new_dataset + ". Press any key to continue.")
    elif item >= 0:
        current_dataset = item

    viewport.window.clear()
    return


initialise()
# try:
main_loop(WindowRectangle(1, 1, 30, 10))
# except:
#     pass
curses.endwin()

We declare that the work we have submitted for this project is entirely our own
work, with the following documented exceptions:

- Caitlin borrowed some ideas from Google, Stackexchange. She documented them within the code wherever she used them.
- Nick used a lot of Caitlin's work so through the transitive property he also borrowed from Google and Stackexchange.

 Much of our work was built to work with ANU's Programs and Courses website so full credit to the ANU for
 producing such a difficult website to parse.


**Signed:** Caitlin Macleod (u5351248), Michael Turvey (u5829484), and Nick Sifniotis (u5809912)
__author__ = 'nsifniotis'

from enum import Enum


class TagTokens(Enum):
    NONE = 0
    TEXT = 1
    EQUALS = 2
    DBQUOTES = 3
    NON_ALPHA = 4
    EOF = -1


class HTMLElement:
    def __init__(self, opening, tag_name, tag_data):
        self.opening = opening      # TRUE if opening tag, FALSE if closing tag
        self.tag_name = tag_name
        self.tag_data = tag_data

    def __repr__(self):
        return "<" + ("" if self.opening else "/") + self.tag_name +\
               (" " + str(self.tag_data) if self.tag_data is not None else "") + ">"


class Tag:
    def __init__(self):
        self.name = ""
        self.children = []
        self.data = dict()

    def set_name(self, name):
        """
        Sets the type of tag tat this is.

        :param name:
        :return:
        """
        self.name = name

    def add_child(self, child):
        if child is not None:
            self.children.append(child)

    def print_self(self, depth):
        print((" " * depth) + "NAME: " + self.name + "  DATA: " + str(self.data))
        for child in self.children:
            if child is not None:
                child.print_self(depth + 2)

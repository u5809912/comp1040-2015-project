__author__ = 'nsifniotis'

import json
from ParsingEngine.SyntaticEngine import HTMLTreeUtils
from ParsingEngine.Tokeniser import *


class Tokeniser:
    def __init__(self):
        self.input_data = ""
        self.master_data = dict()
        self.current_token = ""
        self.current_token_type = TagTokens.NONE

    def set_input(self, input_string):
        self.input_data = input_string
        self.current_token_type = TagTokens.NONE

    def next(self):
        """
        Very simple string tokeniser for processing HTML tags
        The only special symbols that it recognises are '=', ' ' and '"'
        :return:
        """
        if self.current_token_type == TagTokens.EOF:
            return

        if self.input_data == "":
            self.current_token_type = TagTokens.EOF
            self.current_token = ""
            return

        counter = 0
        token_found = False
        self.current_token = ""
        state = 0
        while counter < len(self.input_data) and token_found == False:
            if state == 0:      # consume leading spaces
                if self.input_data[counter] != " ":
                    state = 1
                else:
                    counter += 1
            elif state == 1:    # what the hell am I looking at here?
                if self.input_data[counter] == "=":        # pretty east. This tag is solo
                    token_found = True
                    self.current_token = "="
                    self.current_token_type = TagTokens.EQUALS
                    counter += 1
                elif self.input_data[counter] == "\"":     # bit tougher. Want to extract the entire string.
                    counter += 1
                    state = 2                               # special 'string extraction' state
                else:
                    state = 3                               # collect an alphanum entity
            elif state == 2:
                if self.input_data[counter] == "\"":
                    self.current_token_type = TagTokens.DBQUOTES
                    token_found = True
                    counter += 1
                else:
                    self.current_token += self.input_data[counter]
                    counter += 1
            elif state == 3:
                if self.input_data[counter] in [" ", "=", "\""]:
                    self.current_token_type = TagTokens.TEXT
                    token_found = True
                else:
                    self.current_token += self.input_data[counter]
                    counter += 1

        if counter == len(self.input_data) and not token_found:                # eof reached, so store whatever the last item was
            if state == 0:
                self.current_token_type = TagTokens.EOF
            elif state == 2:
                # this is prety fucked. Missing a closing quotation mark
                self.current_token_type = TagTokens.NON_ALPHA
            elif state == 3 and self.current_token != "":
                self.current_token_type = TagTokens.TEXT

        self.input_data = self.input_data[counter:]


    def consume_string(self, input_line):
        """
        Returns the next tag, which is guaranteed to be a TEXT type tag because the first character
        in input_line is guaranteed not to be a tag opening symbol '<'

        :param input_line: The data that is being parsed
        :return:
        """
        result = Tag()
        result.name = "TEXT"

        start_tag_location = input_line.find("<")
        if start_tag_location == -1:
            result.data["text"] = input_line.strip()
            input_line = ""
        else:
            result.data["text"] = input_line[0:start_tag_location].strip()
            input_line = input_line[start_tag_location:]

        return result, input_line

    def consume_tag(self, input_line):
        """
        Consumes the next tag in this string.
        Returns a triple; the tag itself, a modifier to the current depth, and the remainder of the string.
        Does another nice thing in that it automatically consumes comment tags.

        :param input_line: the string to process
        :return:
        """
        if "<!--" == input_line[0:4]:
            # it's a comment, ignore it.
            location = input_line.find("-->")
            input_line = input_line[location+3:]
            return None, 0, input_line

        tag_type = True
        tag_end_position = input_line.find(">")
        if tag_end_position == -1:
            return None, 0, ""

        tag_string = input_line[1:tag_end_position]
        if tag_string[0] == '/':
            tag_type = False
            tag_string = tag_string[1:]

        self.set_input(tag_string)
        self.next()
        if self.current_token_type == TagTokens.TEXT:
            tag_name = self.current_token
            self.next()
        else:
            print("Parse error finding tag name in string " + tag_string)
            return

        tag_data = dict()
        while self.current_token_type is not TagTokens.EOF:
            if self.current_token_type is not TagTokens.TEXT:
                print("Parse error - unexpected input found on line. " + tag_string)
                print(str(self.current_token_type))
                return
            else:
                data_item_name = self.current_token
                self.next()
                if self.current_token_type == TagTokens.EQUALS:
                    self.next()
                    if self.current_token_type == TagTokens.TEXT or self.current_token_type == TagTokens.DBQUOTES:
                        data_value = self.current_token
                        tag_data[data_item_name] = data_value
                        self.next()
                    else:
                        print("Parse error - unexpected input found on line. " + tag_string)
                        print(str(self.current_token_type))
                        return
                elif self.current_token_type == TagTokens.TEXT or self.current_token_type == TagTokens.EOF:
                    tag_data[data_item_name] = 1
                else:
                    print("Parse error - unexpected input found on line. " + tag_string)
                    print(str(self.current_token_type))
                    return

        result = HTMLElement(tag_type, tag_name, tag_data)

        depth = 2 if tag_type else -2
        input_line = input_line[tag_end_position+1:]

        # tags that we are ignoring
        ignore_tags = ["br", "img", "meta", "link", "input"]
        if tag_name in ignore_tags:
            result = None
            depth = 0

        return result, depth, input_line


class Stripper:
    def matching_tags(self, input_line, current_depth, output_file, tokeniser):
        """

        :param input_line:
        :param current_depth:
        :param output_file:
        :return:
        """
        opening_tag, depth_modifier, input_line = tokeniser.consume_tag(input_line)
        if opening_tag is None:
            # this actually means that the parser found a tag that we are ignoring.
            # so ignore it
            return None, input_line

        tag_to_return = Tag()
        tag_to_return.set_name(opening_tag.tag_name)
        tag_to_return.data = opening_tag.tag_data

        output_file.write((' ' * current_depth) + str(opening_tag) + "\n")
        # print("OPENING: " + (' ' * current_depth) + str(opening_tag))
        current_depth += depth_modifier

        # this is an unbelievably bad hack
        # way too many hacks in this piece of work
        we_are_list_item = True if opening_tag.tag_name == "li" else False

        match_found = False
        while match_found is False:
            if len(input_line) == 0:
                print("Parse error: Out of input!")
                return None, ""

            if input_line[0] != '<':
                tag, input_line = tokeniser.consume_string(input_line)
                if tag is not None:
                    # print("INNER  : " + (' ' * current_depth) + tag)
                    output_file.write((' ' * current_depth) + str(tag) + "\n")
                    tag_to_return.children.append(tag)
            elif input_line[0:4] == "<!--":
                # it's a comment, ignore it.
                location = input_line.find("-->")
                input_line = input_line[location+3:]
            elif input_line[1] != '/':
                if we_are_list_item and input_line[0:3] == "<li":
                    input_line = "</li>" + input_line
                    match_found = True
                else:
                    child_tag, input_line = self.matching_tags(input_line, current_depth, output_file, tokeniser)
                    tag_to_return.add_child(child_tag)
            else:
                match_found = True

        closing_tag, depth_modifier, input_line = tokeniser.consume_tag(input_line)
        if closing_tag is None:
            print("Parse error location 2 closing tag - " + input_line)
            return None, input_line

        if closing_tag.tag_name != opening_tag.tag_name:
            # be bold and create the missing tag
            # this could either fuck us over or seriously repair the broken HTML
            # which will it be? Who knows.
            print("UNBALANCED TAGS: OPENING " + str(opening_tag) + ": CLOSING " + str(closing_tag))
            print("Rebalancing with additional " + str(opening_tag))
            input_line = str(closing_tag) + input_line
            closing_tag = HTMLElement(False, opening_tag.tag_name, "")

        current_depth += depth_modifier
        # print("CLOSING: " + (' ' * current_depth) + str(closing_tag))
        output_file.write((' ' * current_depth) + str(closing_tag) + "\n")

        return tag_to_return, input_line

    def strip_collections(self, tokeniser):
        path_from = "../Scraper/TempDownloads/Collections/"
        path_to = "../Scraper/StrippedData/Collections/"

        with open('../Scraper/Lists/Collections.json') as list_file:
            list_data = json.load(list_file)
        for collection_code, collection_data in list_data.items():
            if "ACCT-MAJ" in collection_code:
                print("Processing " + collection_code)

                input_file = open(path_from + collection_code + ".html", "r")
                output_file = open(path_to + collection_code + ".temphtml", "w")
                full_string = ""
                state = 0

                for line in input_file:
                    line = line.strip()
                    if line != "":
                        line = line.replace("&nbsp;", "")

                        full_string += line

                # the first tag is always a rubbish tag
                tag, rubbish, full_string = tokeniser.consume_tag(full_string)
                output_tag, empty_string = self.matching_tags(full_string, 0, output_file, tokeniser)

                input_file.close()
                output_file.close()
        return output_tag

s = Stripper()
tk = Tokeniser()
html_tree = s.strip_collections(tk)

x = HTMLTreeUtils()
x.remove_single_children(html_tree)

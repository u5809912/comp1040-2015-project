__author__ = 'nsifniotis'

from ParsingEngine.Tokeniser import Tag


class HTMLTreeUtils:
    def remove_single_children(self, html_tree):
        """
        Prunes the HTML tree by compressing any nodes that have only one child, where that child is not
        a special text node.

        :param html_tree:
        :return: The reduced HTML tree
        """

        self.__prune_unwanted_tags(html_tree)

        differential = -1
        while differential != 0:
            differential = 0
            original = self.__generate_tree_stats(html_tree)
            self.__reduce_tree_recurser(html_tree)
            reduction = self.__generate_tree_stats(html_tree)

            for key in original:
                o_val = original[key]
                if key in reduction:
                    r_val = reduction[key]
                else:
                    r_val = 0
                differential += r_val - o_val

        html_tree.print_self(0)

    def print_tree_statistics(self, html_tree):
        """
        Generates some statistics about the HTML tree that is being processed.
        This will be used to analyse the effects of modifying the tree structure.

        :param html_tree:
        :return: Nothing. This function dumps everything to the console.
        """
        tag_type_counter = self.__generate_tree_stats(html_tree)

        print("** Tag type counts **")
        keys_array = tag_type_counter.keys()
        keys_array = sorted(keys_array)
        for key in keys_array:
            print(key + ": " + str(tag_type_counter[key]))

    def __generate_tree_stats(self, html_tree):
        """
        Recursive method for extracting the information that we want from the HTML tree.

        :param html_tree:
        :return:
        """

        tag_type_counter = dict()
        name = html_tree.name
        tag_type_counter[name] = 1

        for child in html_tree.children:
            if child is not None:
                holder = self.__generate_tree_stats(child)
                for key in holder.keys():
                    if key in tag_type_counter.keys():
                        tag_type_counter[key] += holder[key]
                    else:
                        tag_type_counter[key] = holder[key]

        return tag_type_counter

    def __prune_unwanted_tags(self, html_tree):
        """
        Get rid of shitty tags like HEAD and SCRIPT, or any tags at all with no data and no children.
        Literally flush them out of the tree.

        :param html_tree:
        :return:
        """
        new_child_list = []
        for child in html_tree.children:
            include = True
            if child.name == "head":
                include = False
            if child.name == "script":
                include = False
            if len(child.children) == 0 and len(child.data.keys()) == 0:
                include = False
            if 'id' in child.data and (child.data['id'] == "explore" or child.data['id'] == "anu-footer"):
                include = False

            if include:
                new_child_list.append(child)

        html_tree.children = new_child_list

        for child in html_tree.children:
            self.__prune_unwanted_tags(child)

    def __reduce_tree_recurser(self, html_tree):
        """
        Recursive function to reduce the HTML tree.

        :param html_tree:
        :return:
        """

        new_child_list = []
        for child in html_tree.children:
            # does this child have only one child?
            # is that child not a text node?
            # if these conditions are satisfied, compress the tree
            if len(child.children) == 1:
                if child.name != "a":
                    new_child_list.append(child.children[0])
                elif child.children[0].name == "TEXT":
                    child.data["TEXT"] = child.children[0].data["text"]
                    child.children = []
                    new_child_list.append(child)
                else:
                    new_child_list.append(child)
            else:
                new_child_list.append(child)

        html_tree.children = new_child_list

        # second swoop - merge neighbouring text nodes into one
        new_child_list = []
        holding_string = ""
        for child in html_tree.children:
            if child.name == "TEXT":
                holding_string += ("" if holding_string == "" else " ") + child.data["text"]
            else:
                if holding_string != "":
                    newtag = Tag()
                    newtag.set_name("TEXT")
                    newtag.data["text"] = holding_string
                    new_child_list.append(newtag)
                    holding_string = ""

                new_child_list.append(child)

        # add the leftover bits so they arent lost
        if holding_string != "":
            newtag = Tag()
            newtag.set_name("TEXT")
            newtag.data["text"] = holding_string
            new_child_list.append(newtag)

        html_tree.children = new_child_list

        # now, force the children to do the same thing
        for child in html_tree.children:
            self.__reduce_tree_recurser(child)
__author__ = 'nsifniotis'

from GUI.WindowComponents import *
from GUI.Utils import *


class MenuItem:
    """
    This class contains all the data used for menu items that are held in the Menu class.
    """
    def __init__(self, item_text, callback, help_text=""):
        self.item_text = item_text
        self.callback = callback
        self.help_text = help_text

    def __repr__(self):
        return self.item_text + ": " + self.help_text

    def get_item_text(self, max_width=10000):
        """
        Returns a truncated representation of the menu item text.
        If no maximum width is provided, return the entire string.

        (Technically, truncated at width 10,000, but that should never be an issue in practice.
        If your menu items are ten thousand characters long, you have bigger problems than
        unexpected truncation)

        :param max_width: The maximum size of the string to return.
        :return: The truncated menu item text.
        """
        return self.item_text if len(self.item_text) <= max_width else (self.item_text[0:max_width-2] + "..")


class Menu(Window):
    """
    Creates a basic menu interface widget for use with curses.
    """
    def __init__(self, window_dimensions, viewport):
        Window.__init__(self, viewport)
        self.set_dimensions(window_dimensions)
        self.set_border_style(WindowBorder.SINGLE)
        self.menu_items = []
        self.current_selection = 0

    def add_menu_item(self, item):
        self.menu_items.append(item)

    def get_menu_choice(self):
        """
        Presents this menu to the user, and returns the value associated with the selected menu item
        back to the owner of this menu.
        """
        choice_made = False
        while not choice_made:
            self.refresh()
            character = self.viewport.window.getch()
            if character == 13:             # the constant curses.KEY_ENTER is not working??
                choice_made = True
            elif character == curses.KEY_LEFT or character == curses.KEY_UP:
                self.current_selection -= 1
                if self.current_selection < 0:
                    self.current_selection = len(self.menu_items) - 1
            elif character == curses.KEY_RIGHT or character == curses.KEY_DOWN:
                self.current_selection += 1
                if self.current_selection >= len(self.menu_items):
                    self.current_selection = 0

        return self.menu_items[self.current_selection].callback

    def refresh(self):
        """
        Draws the menu on the console.
        """
        Window.refresh(self)

        counter = 0
        for menu_item in self.menu_items:
            attributes = curses.color_pair(self.viewport.HIGHLIGHT) \
                if counter == self.current_selection else curses.color_pair(self.viewport.DEFAULT)
            self.viewport.window.addstr(self.y1 + counter + 1, self.x1 + 1,
                                        truncate_string(menu_item.item_text, self.width - 2), attributes)
            counter += 1

        self.viewport.window.refresh()

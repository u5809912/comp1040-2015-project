__author__ = 'nsifniotis'

from GUI.Utils import *
from enum import Enum
import curses


class WindowRectangle:
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height


class WindowBorder(Enum):
    NONE = 0
    SINGLE = 1
    DOUBLE = 2


class Window:
    def __init__(self, viewport):
        self.viewport = viewport
        self.has_focus = True
        self.x1 = 0
        self.y1 = 0
        self.x2 = 0
        self.y2 = 0
        self.width = 0
        self.height = 0
        self.title = ""
        self.border_style = WindowBorder.NONE

    def set_dimensions(self, dimensions):
        """
        Note that the refresh / redraw code does not correctly handle window resizes.
        Not by design anyway. It could well do it by accident.
        """
        self.x1 = dimensions.x
        self.y1 = dimensions.y
        self.width = dimensions.width
        self.height = dimensions.height
        self.x2 = dimensions.x + dimensions.width
        self.y2 = dimensions.y + dimensions.height

    def set_border_style(self, border):
        self.border_style = border

    def set_title(self, title):
        self.title = title

    def get_focus(self):
        self.has_focus = True

    def lose_focus(self):
        self.has_focus = False

    def refresh(self):
        for i in range(self.y1, self.y2):
            self.viewport.window.addstr(i, self.x1, " " * self.width, curses.color_pair(self.viewport.DEFAULT))

        if self.border_style == WindowBorder.SINGLE:
            for i in range(self.y1, self.y2):
                self.viewport.window.addstr(i, self.x1, "|", curses.color_pair(self.viewport.DEFAULT))
                self.viewport.window.addstr(i, self.x2, "|", curses.color_pair(self.viewport.DEFAULT))
            for i in range(self.x1, self.x2):
                self.viewport.window.addstr(self.y1, i, "-", curses.color_pair(self.viewport.DEFAULT))
                self.viewport.window.addstr(self.y2, i, "-", curses.color_pair(self.viewport.DEFAULT))

        working_title = truncate_string(self.title, self.width - 2)
        self.viewport.window.addstr(self.y1, self.x1 + 1, working_title, curses.color_pair(self.viewport.HIGHLIGHT))

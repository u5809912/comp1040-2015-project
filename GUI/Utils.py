__author__ = 'nsifniotis'

import curses


class Viewport:
    def __init__(self, window):
        self.HEIGHT, self.WIDTH = window.getmaxyx()
        self.DEFAULT = 1
        self.HIGHLIGHT = 2
        self.ALERT = 3
        self.STATUS_OK = 4
        self.STATUS_NOK = 5
        self.window = window


def alert(viewport, prompt):
    """
    Displays an alert message at the bottom of the screen. They are usually 'press any key to continue'
    messages, but can also be used to accept one-character commands from the user.

    :param viewport: The current viewport.
    :param prompt: The message to display.
    :return: The character that the user entered to clear the message. Its use is strictly optional.
    """

    # display the message
    prompt = truncate_string(prompt, viewport.WIDTH - 2, True)
    viewport.window.addstr(viewport.HEIGHT - 1, 0, prompt, curses.color_pair(viewport.ALERT))
    viewport.window.refresh()

    # get the user input
    garbage = viewport.window.getch()

    # restore the screen to a no-alert state
    viewport.window.addstr(viewport.HEIGHT - 1, 0, " " * (viewport.WIDTH - 2), curses.color_pair(viewport.DEFAULT))
    viewport.window.refresh()

    return garbage


def status_message(viewport, string):
    """
    Updates the status bar with a message.

    :param viewport: The current viewport.
    :param string: The message to display.
    """

    # display the message
    prompt = truncate_string(string, viewport.WIDTH - 2, True)
    viewport.window.addstr(viewport.HEIGHT - 2, 0, prompt, curses.color_pair(viewport.DEFAULT))
    viewport.window.refresh()
    return


def truncate_string(string, max_len, pad = False):
    """
    Truncates a string to the specified length.

    :param string: The string to truncate.
    :param max_len: The maximum accepted width of the string.
    :param pad: If set to true, fil lthe string with whitespace to make it reach max_len.
                Guarantees to return a string of length max_len
                Set to False by default.
    :return: The truncated string.
    """
    res = string

    if pad and len(string) < max_len:
        res = string + (" " * (max_len - len(string)))
    elif len(string) > max_len:
        res = string[0:max_len-2] + ".."

    return res
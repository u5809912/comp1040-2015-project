We declare that work for our project was distributed among the group members,
and the main contributions were as follows:

* Nick Sifniotis (u5809912) --- 60%
 - Developed the program parser (not completed in time)
 - Developed the major/minor parser, which is working, but not necessarily completely working.
   There hasn't been time to test it.
 - Designed the internal data representation of courses and their prerequisites.
 - Pivoted to ElectiveMate when it became apparent that the main software would not be completed in time.


* Caitlin McLeod (u5351248) --- 20%
 - Designed structure for installer, parser code.
 - Helped develop internal data representation
 - JSON champion

* Michael Turvey (u5829484) --- 20%
 - Worked a lot on the courses parser. Unfortunately that part of the software was not completed in time.
 - User experiece guru. Had the big picture / vision for the software.

There were some issues in getting this software complete in time for the deadline. All three group members are
extremely busy people and it was challenging finding the time to meet and work on this project. As the deadline
approached and it became apparent that we would not get this show on the road, Nick quickly developed a proof of
concept for a smaller app that solves a related problem to the main task. That little app is ElectiveMate.

So we have a working proof of concept for a much smaller subproblem than our original task.


In the end, parsing Programs and Courses just proved too difficult a job in the time that we had available.
# Title
Degree Planner

# Group Members

- u5351248 Caitlin Macleod
- u5829484 Michael Turvey
- u5809912 Nick Sifniotis

# Overview
The purpose of the Degree Planner is to provide a given student a list of courses they should take based on their degree, courses they want to do, and courses they do not want to do. The user will be offered a plan which contains as many of their desired courses as possible, as few of their non-desired courses, and which satisfies the conditions for their degree.
The Degree Planner will be presented as a webpage, we will use python as the backend language to process the information, and javascript on the front-end. This will enable users easy access to the planner.
Difficulties will be faced in data collection; we will attempt to gain access to the original data source but it is more likely we will have to scrape the data from the Programs and courses website. We may also encounter difficulties with complex degree structures. Our current plan is to begin with single degrees, and include flexible double degrees only if we have extra time. We will begin with degrees which have limited choice and no major/minor structure such as BACMP(H)or PPE.
Given the extra complexity of negotiating which years and which semester courses run, and given many courses do not list this data online, we have decided to ignore semester and year, on the assumption that given a list of subjects, the user can negotiate which subjects to do in which year. If we have leftover time we will include a condition to ensure we do not provide too many courses in one semester, however given that most courses will be operating with one or more prerequisites, and that these prerequisites are structures such that there is an obvious progression through semesters, this should not be a major issue.
In the event that this project proves too complex or time-consuming, we will consider scaling back the project to a textual and graphical representation of the prerequisites, majors, minors and degree structures of ANU, based on the same data.
The program itself will take course inputs, determine in which majors and minors they are a part and determine and prerequisites for the course. The program will choose both any major or minor as required, and the courses included, based on comparison of a numerical estimate of the suitability of the major, minor or course; i.e. a course with two prerequisites that are also part of the User's 'want to do' courses will score higher than a course with two prerequisites the user has not selected. In this way the optimal degree plan will be produced.
# Data
<!--- A description of where you will get your data. --->
We will probably be implementing a scraper to scrape degree and course information from the Programs and courses website.
To minimise duplication of effort, we will get in touch with the P&C admin to see if there's an API that we can be granted access to, but most likely we'll just use a scraper. The searches seem to be easy to define, and you can search for the entire list of programs at once (albeit by pressing the "see more" button. For the most part it will probably be easiest to limit the search to undergraduate, single degrees and majors, minors and specialisations. Caitlin has some experience with python and javascript so this shouldn't be too hard.

## Information that we *need*

- list of courses (course code, course name, years+semesters running, unit value, college offering, prerequisites)
- list of majors, minors and specialisations (code, name, list of required course codes, list of possible course codes, how many units required from list of possible courses)
## Information that would be nice

- list of programs (degrees) with information about what courses are required and how many from different areas. this is vague and hard to get, there are so many requirements.
- postgraduate courses and majors (do postgraduate degrees have majors?)

## Input data from users

- courses already taken
- degree in which they are enrolled
- courses student wants to take
- courses student does not want to take

# Project Milestone
By the project milestone we will be able to scrape all data we need for the Degree Planner, and assign individual courses their various conditions (major, minor, prerequisites)

# Final Deliverable
The final project will be a web page in which students at ANU can input courses they have taken, courses they want to take and courses they don't want to take. They will then be returned the list of courses they should complete for their personal best possible degree.

# Programming Language
We will be using Python for the data processing, and JavaScript + HTML to present the input and output on a webpage for student access.

# References
<!---Any preliminary references that may be relevant for your project. --->
[http://programsandcourses](http://programsandcourses.anu.edu.au) - the place where degree and course structure is listed. The administrator can be contacted at programsandcourses@anu.edu.au
[http://anutimetable.com/](http://programsandcourses.anu.edu.au) - a nice open source project that scraped course and timetable information from timetable.anu.edu.au
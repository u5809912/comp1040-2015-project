ElectiveMate

A proof of concept of a neat little app to help ANU students.


Introduction
Like many students, the biggest problem I face at the start of the year is picking my electives. What courses can I
take? Can I do these courses with the ones that I have already completed? Why can't I find a list of available electives?

Introducing ELECTIVE-MATE, your one stop shop for all things electives.

With ELECTIVE-MATE, picking your courses is as simple as listing the courses you've already done and letting the
computer do the rest. ELECTIVE-MATE searches through the entire database of ANU courses and picks out the ones that
you can do, so you don't have to!

WOW

But how does it do that, I hear you ask? With the power of PYTHON PROGRAMMING! (full technical details are available below)


Installation

Step 1: Download the repo.

Step 2: Open a terminal and navigate to the root directory of the repo that you just downloaded.

Step 3: Execute the installer by typing the following into the command line:
"python __main__.py -i"

The installer will then connect to the ANU database and download all the information it needs to make choosing electives
a fun and enjoyable experience!

Be warned: It downloads a lot of data. May take up to an hour depending on your internet connection.

Step 4: Since this is a proof of concept app, you will need to execute a script to manually insert course data.
Type the following into the command line:
"python ManualCourses.py"

Step 5: There is no step 5. Assuming that you received no error messages during the installation process, enjoy
the POWER and FREEDOM that comes with being able to choose your electives!!


Usage

This is a PROOF OF CONCEPT app, which means that it demonstrates that the idea is sound, without necessarily running
with a full dataset. This app is restricted to COMP electives only, however it will accept ANY units that you have
already completed at ANU.

To start the program, type the following into the command line:

"python ElectiveMate.py"

ELECTIVE-MATE will start and it will prompt you for a list of courses that you have completed at ANU. Please enter them
one at a time, by course code. For example, "comp1040" and "COMP100" are valid entries, but "The Craft of Computing"
is not. Don't worry if you make a mistake, ELECTIVE-MATE is forgiving and will let you try again!

When you have entered all of the courses that you completed, simply press <ENTER> and let the ** magic ** of
PYTHON PROGRAMMING do its work.

In a matter of moments, ELECTIVE-MATE will produce a list of every (COMP) course that you are able to enrol in,
based on the list of courses that you have already completed. WOW! No more hunting around trying to find course lists!

It will produce a list of course names and codes. Some courses at ANU have special conditions attached to them, and
these will be displayed alongside the courses they affect. For example, many of the later-year project courses have
no prerequisites or incompatibles per se, but a 'permission code' is required from an instructor before you can enrol.


But what happened to the Programs and Courses chooser that we were working on?

We ran out of time. Programs and Courses is an absolute dog of a website. It's unparsable. Advanced machine learning
techniques are needed - or manually parsing over 3,000 programs and courses. It was simply not feasable.

Our attempts to do the impossible are sitting in the repo. We do not guarantee that these scripts are safe to run,
but feel free to poke around if you feel like it.


Enjoy the flexibility and outstandingness of ELECTIVE-MATE.

Caitlin, Michael and Nick
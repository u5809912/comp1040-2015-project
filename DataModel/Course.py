__author__ = 'michaelturvey'
import re
from urllib import request
from urllib.error import URLError
from bs4 import BeautifulSoup, NavigableString, Tag
from DataModel.CourseList import *

class Course:
    """
    Caitlin Macleod u5351248
    30/9/15

    Based somewhat on DataModel.Program which was written by Nick Sifniotis u5809912

    Contains everything that a course needs to know about it,
    namely Name, Code, Unit cost, Prereqs and incompatibilities, Majors, Minors and Specialisations it belongs to.
    """

    def __init__(self, data_source):
        """
        Caitlin Macleod u5351248
        30/9/25

        :param data_source: The object returned by JSON data from list of courses search
        :return nothing: this is a constructor
        """
        self.course_name = data_source['course_name']
        self.course_code = data_source['course_code']
        self.subject_code = Course.__get_subject_code(data_source['course_code'])
        self.level = Course.__get_level(data_source['course_code'])
        self.college        = ""
        self.school         = ""
        self.unit_cost      = int(data_source['unit_cost'])
        self.prerequisites = {'incompatible':[],"required_codes":[],"required_complete":{"units":"","level":"","area":""}} #TODO
        self.collections_belonged_to = [] #TODO

        # sorry Michael, dont mean to tread on your toes here
        self.incompatible_courses = []
        self.required_courses = []
        self.special_conditions = []

        self.price = []
        self.links = []
        self.text = []
        self.tables = []
        self.incompletes = []
        #This doesn't work and I have no clue why
        #self.filename = "DataModel/Courses/"+self.course_code+".html"
        #newfile = open(self.filename, "w+")
        self.filename = data_source['filename']
        self.Failure = False

    @classmethod
    def from_search_item(cls, json_collection_input):
        """
        Nick Sifniotis u5809912
        28/10/2015

        Initialise a Course object from the result that is given by the raw search json linked to at the top.

        :param json_collection_input: the raw JSON data
        :return:
        """
        data = dict()
        data['course_name'] = json_collection_input['Name']
        data['course_code'] = json_collection_input['CourseCode']
        data['unit_cost'] = json_collection_input['Units']
        data['filename'] = "TempDownloads/Courses/" + data['course_code'] + ".html"
        return cls(data)

    @classmethod
    def from_json_list(cls, json_data):
        """
        Nick Sifniotis u5809912
        28/10/2015

        Initialise this course from data saved by the scraper in Scraper/Lists

        :param json_data: the JSON data to load from
        """
        return cls(json_data)

    @classmethod
    def from_parsed_data(cls, json_data):
        """
        Nick Sifniotis u5809912
        29/10/2015

        A more advanced initialiser, this one creates the complete Course object using the parsed data.
        It works by initialising the trivial data and then constructing the incompatible and required course lists.

        :param json_data: the data to load from
        :return: a new instance of this class
        """
        result = cls(json_data)
        incompatibles = json_data['incompatible_courses']
        requireds = json_data['required_courses']
        result.special_conditions = json_data['special_conditions']

        for incomp in incompatibles:
            new_list = FiniteList.from_json(incomp) if "course_list" in incomp else InfiniteList.from_json(incomp)
            result.incompatible_courses.append(new_list)

        for reqd in requireds:
            new_list = FiniteList.from_json(reqd) if "course_list" in reqd else InfiniteList.from_json(reqd)
            result.required_courses.append(new_list)

        return result

    def __str__(self):
        """
        Nick Sifniotis u5809912
        28/10/2015

        :return: A string representation of this course.
        """
        conditions_tag = ""
        if self.special_conditions:
            conditions_tag = " [Special Conditions"
            for condition in self.special_conditions:
                conditions_tag += "  - " + condition
            conditions_tag += "]"

        return self.course_code + ": " + self.course_name + conditions_tag
    
    def test_failure(self):
        try:
            open(self.filename,'r')
        except FileNotFoundError:
            #print("FAILURE")
            self.Failure = True
        else:
            html_data = open(self.filename,'r')
            try:
                BeautifulSoup(html_data)
            except UnicodeDecodeError:
                #print("FAILURE")
                self.Failure = True

    def download_course_data(self):
        """
        Caitlin Macleod u5351248
        30/9/15
        This downloads and saves the HTML for the course from programsandcourses.
        Calling this is recommended for first time.

        :return: nothing
        """
        url = "http://programsandcourses.anu.edu.au/course/" + self.course_code

        try:
            print(" Done!")
            return request.urlretrieve(url, self.filename)
        except URLError:
            print(" Tried to download collection data from " + url +
                  " but failed. Setting " + self.code + " to be ignored.")
            self.Failure = True

    def parse_course_data(self, repo):
        """
        Michael Turvey u5829484
        6/10/2015

        Launches the fury of a thousand armies on the poor innocment HTML file
        Attempts to assemble some data from the dismal remnants.
        :return: Whatever doesn't parse
        """
        print(self.course_code)
        html_data = open("../Scraper/" + self.filename,'r')
        raw_data = BeautifulSoup(html_data)
        #STORE ALL LINKS
        for link in raw_data.find_all('a'):
            self.links.append(link.get('href'))
        #print(self.links)

        #GET COLLEGE TEXT
        for text in raw_data.find_all('p','span'):
            #print(text.get_text())
            self.text.append(text.get_text())
        #print(self.college,self.school)

        #STORE ALL TABLES
        for table in raw_data.find_all("table"):
            self.tables.append(table.get_text())
            #print(table.get_text())
            #print(self.tables)
        self.parse_tables()
        #GET REQUISITES
        req = raw_data.find(class_ = "requisite")
        if req != None:
             self.parse_requisites(req.get_text())
    def parse_text(self):
        """ Michael Turvey u5829484
        11/10/15
        Parses text data from html file to find college, school
        :return: nothing
        """
        for text in self.text:
            #print(text)
            if "College of" in text:
                self.college = text[15:]
                #print(self.college)
            if "School of" in text:
                self.school = text[10:]
                #print(self.school)
    def parse_tables(self):
        """ Michael Turvey u5829484 11/10/15
        Grabs unit price from the text
        :return:Nothing
        """
        for item in self.tables:
            if "$" in item:
                item = item.strip('\n')
                item = item.split("$")[1]
                self.price.append(item)
        #print(self.price)


    def parse_requisites(self,input):
        """
        Michael Turvey u5829484 11/10/2015
        This is a tricky one. Parses prerequisite info to return a list of prerequisite courses.
        :param requisites: The string containing info about prerequisites
        :return:nothing
        """
        #requisites.lower()
        requirements = ''
        completion_con = ''
        processed_string = ''
        requisite_list = []
        #==============Format requisite strings to standardise as much as possible============================
        requirements = input
        requirements = ':'+requirements
        #==============Mark requirements======================
        #TODO all of the formatting
        #Standardise grammar
        requirements = re.sub('\.',':FULLSTOP:',requirements)
        requirements = re.sub('\,',':COMMA:',requirements)
        #Permission of Convener
        requirements = re.sub(':[^:]* permission[^:]*',"::PERMISSION:",requirements)
        #Standardise "units from" requirements
        requirements = re.sub(" *([0-9]*) [Uu]nits of ([0-9]*).level",r":\1 \2:",requirements)
        #Standardise course from school requirements
        requirements = re.sub("[\S]* \(([A-Z]{4})\)",r"\1",requirements)
        requirements = re.sub("([A-Z]{4}) [Cc]ourses",r":\1:",requirements)
        requirements = re.sub(" *([A-Z]{4}) +",r':\1:',requirements)
        #Requirement qualifiers
        requirements = re.sub('To enrol in this course you must ',"REQ:",requirements)
        requirements = re.sub('have ?[\S]* completed',":COMPLETE:",requirements)
        requirements = re.sub('be studying',":STUDY:",requirements)
        requirements = re.sub(':[^:]*no.*enrol.*COMPLETE:',":INCOMP:",requirements)
        #Ands and Ors
        requirements = re.sub('[:, ]or ',":OR:",requirements)
        requirements = re.sub(' and ',":AND:",requirements)
        #==============Divide string into parts===============
        #TODO divide
        #==============Store requirements==============
        #TODO store divided chunks of requirement string in data structure
        #=============Return unparseable chunks===============
        #TODO return these
        processed_string = requirements
        print(processed_string)
        print(input)
    def is_code(self, text):
        """ Michael Turvey u5829484 15/10/15
        Checks if a string contains a course code
        :param text: input string
        :return: true or false
        """

    @classmethod
    def __get_subject_code(cls, course_code):
        res = ""
        counter = 0
        while counter < len(course_code) and course_code[counter].isalpha():
            res += course_code[counter]
            counter += 1

        return res

    @classmethod
    def __get_level(cls, course_code):
        """
        Nick Sifniotis u5809912
        29/10/2015


        :param course_code:
        :return:
        """
        res = 0
        counter = 0
        while counter < len(course_code) and course_code[counter].isalpha():
            counter += 1

        res = int(course_code[counter]) * 1000

        return res

    def check_requirements_satisfied(self, completed_course_list):
        res = True

        if self.__am_i_in_list(completed_course_list):
            res = False
        else:
            # check off the completed list against both the required and incompatible lists.
            for reqd in self.required_courses:
                res &= reqd.check_course_list(completed_course_list)

            for excd in self.incompatible_courses:
                res &= excd.check_course_list(completed_course_list)

        return res

    def __am_i_in_list(self, course_list):
        """
        Nick Sifniotis u5809912
        29/10/2015

        Return true if this instance is found within the list of courses.

        :param course_list: the list of courses to search
        :return: true if this object is amongst them
        """
        res = False
        for item in course_list:
            res |= item.course_code == self.course_code

        return res
__author__ = 'nsifniotis'


from abc import ABCMeta, abstractmethod
from DataModel.Functions import *

class CourseList:
    __metaclass__ = ABCMeta

    @abstractmethod
    def check_course_list(self, courses_to_check): pass


class ANDList(CourseList):
    """
    Nick Sifniotis u5809912
    02/11/2015

    Represents an 'AND' boolean function
    Returns true if all of this node's children are satisfied
    by the course list that is being checked.
    """
    def __init__(self):
        self.children = []

    def add_child(self, child):
        self.children.append(child)

    def check_course_list(self, courses_to_check):
        res = True

        for child in self.children:
            res &= child.check_course_list(courses_to_check)

        return res

class ORList(CourseList):
    """
    Nick Sifniotis u5809912
    02/11/2015

    Represents an 'OR' boolean function
    Returns true if at least one of the child nodes is
    satisfied by the course list being tested.
    """
    def __init__(self):
        self.children = []

    def add_child(self, child):
        self.children.append(child)

    def check_course_list(self, courses_to_check):
        res = False

        for child in self.children:
            res |= child.check_course_list(courses_to_check)
        return res


class FiniteList(CourseList):
    def __init__(self, num_units, list_type, num_type=False):
        """
        Nick Sifniotis u5809912
        27/10/2015

        Constructor for this Finite object.

        :param num_units: the number of units that the parser read from the HTML
                          that looks like this - "96 units from the following units:"
        :param list_type: whether or not this list represents a minimum number of units,
                          a maximum number of units or an exact number of units.
                          temprarily a string ["MIN", "MAX", "EXACT"] - an enumeration would be better
        :param num_type:  num_units could also be num_courses. If num_type is True, then num_units represents
                          the number of courses that need to be taken, not the number of units.
                          This subtlety is important when you consider that not every course is worth 6 units.
        """
        self.course_list = []
        self.num_units = num_units
        self.list_type = list_type
        self.total_units_in_list = 0
        self.total_courses_in_list = 0
        self.num_type = num_type

    @classmethod
    def from_json(cls, json_data):
        """
        Nick Sifniotis u5809912
        29/10/2015

        Quick and dirty little initialiser from JSON data

        :param json_data:
        :return: a new instance of the class
        """
        result = cls(int(json_data['num_units']), json_data['list_type'], json_data['num_type'])
        result.course_list = json_data['course_list']
        result.total_units_in_list = json_data['total_units_in_list']

        return result

    def add_courses(self, course_list, repo):
        """
        Nick Sifniotis u5809912
        06/11/2015

        Utility function for adding a block of courses in the one go.

        :param course_list: the courses to add
        :param repo: the current data repo
        """
        for course in course_list:
            self.add_course(course, repo)

    def add_course(self, course, repo):
        """
        Nick Sifniotis u5809912
        27/10/2015

        Adds the given course to this FiniteList's list of courses.
        :param course: the course to add
        """
        course_type, course_object = repo.identify_me(course, "CRS")
        if course_object is None or course_type != "CRS":
            return

        self.course_list.append(course)
        self.total_units_in_list += course_object.unit_cost
        self.total_courses_in_list += 1

    def check_course_list(self, course_list):
        """
        Nick Sifniotis u5809912
        29/10/2015

        Checks the courses in this objects list against the courses in the parameter course_list
        If the conditions are satisfied, return true
        :param course_list: the list of courses to check
        :return: true if the conditions are satisfied, false otherwise.
        """

        total_units_found = 0
        total_courses_found = 0
        for course in course_list:
            for item in self.course_list:
                if course.course_code == item:
                    total_units_found += course.unit_cost
                    total_courses_found += 1

        found_count = total_courses_found if self.num_type else total_units_found

        res = False
        if self.list_type == "EXACT":
            res = found_count == self.num_units
        elif self.list_type == "MIN":
            res = found_count >= self.num_units
        elif self.list_type == "MAX":
            res = found_count <= self.num_units
        else:
            print("Critical error: Bad data found in infinite list.")
            exit(1)
        return res

    def check_consistency(self):
        """
        Nick Sifniotis u5809912
        27/10/2015

        This function checks the consistency of the course list that makes up this
        FiniteList of courses. If there exists a pair of courses such that one is listed as
        an incompatible or excluded course of the other, return false otherwise return true.

        It also verifies that the number of units that make up this course list
        is sufficient to meet the number of units that the parser read from the source HTML.

        :return: true if the course list is internally consistent, false otherwise.
        """
        if self.total_units_in_list < self.num_units:
            return False

        result = self.__compare_courses_to_exclusions(self.course_list)
        return not result

    def check_list_excluded(self, courses_to_check):
        """
        Nick Sifniotis u5809912
        27/10/2015

        Checks this FiniteList against the list of courses passed to this function
        to determine whether that course list contains courses that are excluded
        or incompatible with any courses that make up this CourseList.

        :param courses_to_check: the list of courses to search through.
        :return: true if courses_to_check contains any courses that are incompatible with a course in this list.
        """
        return self.__compare_courses_to_exclusions(courses_to_check)

    def __str__(self):
        type_part = "Exactly"
        if self.list_type == "MAX":
            type_part = "Maximum"
        elif self.list_type == "MIN":
            type_part = "Minimum"

        list_part = ""
        for course in self.course_list:
            list_part += " " + course

        return type_part + " " + str(self.num_units) + " units from [" + list_part + " ]"

    def __compare_courses_to_exclusions(self, course_list):
        """
        Nick Sifniotis u5809912
        27/10/2015

        Checks the given course list against the exclusions found in the courses
        that make up this FiniteList. Returns true if a match is found, false otherwise

        :param course_list: the list of courses to search through
        :return: true if a match is found
        """
        result = False

        for course in course_list:
            result |= check_for_exclusions(course, self.course_list)

        return result


class InfiniteList(CourseList):
    """
    Nick Sifniotis u5809912
    28/10/2015

    Represents a generic set of courses described by their level, subject code, which
    college offers the course etc.

    The idea is to encapsulate information that can be used to resolve conditions such as
    'must complete 12 units of 3000 level courses' or 'maximum 60 courses from the subject area COMP'
    """
    def __init__(self, num_units, list_type):
        self.college = None
        self.level = None
        self.subject_code = None
        self.num_units = num_units
        self.list_type = list_type

    @classmethod
    def from_json(cls, json_data):
        """
        Nick Sifniotis u5809912
        29/10/2015

        Quick little object initialiser from JSON data

        :param json_data: the data to load from
        :return: a new instance of the class
        """
        result = cls(int(json_data['num_units']), json_data['list_type'])
        result.college = json_data['college']
        result.subject_code = json_data['subject_code']
        result.level = json_data['level']
        return result

    def check_course(self, course_to_check):
        """
        Nick Sifniotis u5809912
        28/10/2015

        Checks to see whether the course that was passed through to this function is a member
        of the set of courses that this list represents.

        :param course_to_check: the course to check
        :return: true if this course is 'one of us', false otherwise
        """
        # todo implement me - pull the tests out of check_course_list
        return True

    def check_course_list(self, course_list):
        """
        Nick Sifniotis u5809912
        29/10/2015

        Checks to see how many of the courses in course_list fit into this infinite list.
        Compares the results of that filtration to the number of units that the list is 'supposed'
        to have and returns true if the conditions are satisfied.

        :param course_list: the list of courses to check
        :return: true if the conditions that this list represents are satisfied, false otherwise.
        """
        total_units_found = 0

        for item in course_list:
            passes_subject_test = False
            if self.subject_code:
                for subject in self.subject_code:
                    if subject == item.subject_code:
                        passes_subject_test = True
            else:
                passes_subject_test = True

            passes_level_test = False
            if self.level:
                for level in self.level:
                    if level == item.level:
                        passes_level_test = True
            else:
                passes_level_test = True

            if passes_subject_test and passes_level_test:
                total_units_found += item.unit_cost

        res = False
        if self.list_type == "EXACT":
            res = total_units_found == self.num_units
        elif self.list_type == "MIN":
            res = total_units_found >= self.num_units
        elif self.list_type == "MAX":
            res = total_units_found <= self.num_units
        else:
            print ("Critical error: Bad data found in infinite list.")
            exit(1)

        return res

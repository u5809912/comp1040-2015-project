__author__ = 'caitelatte'

from urllib import request
from urllib.error import URLError
from DataModel.CourseList import FiniteList
from bs4 import BeautifulSoup, NavigableString, Tag


class Collection:
    """
    Caitlin Macleod u5351248
    06/10/2015

    Based on DataModel.Program by Nick Sifniotis

    Contains all information that a collection (a major, minor or specialisation) needs to know.
    """

    def __init__(self, json_collection_input):
        """
        Caitlin Macleod u5351248
        06/10/2015

        Initialise a Collection object, storing Code, Name, Units and Type

        :param json_collection_input: a dictionary of the basic information that a collection needs.
        :return: null
        """
        self.name = json_collection_input['name']
        self.code = json_collection_input['code']
        self.type = json_collection_input['type']
        self.units = json_collection_input['units']
        self.ignoreme = json_collection_input['ignoreme']
        self.filename = json_collection_input['filename']
        self.course_lists = []


    @classmethod
    def fromlistjson(cls, list_collection_input):
        """
        Caitlin Macleod u5351248
        07/10/2015

        Initialise a Collection object from the basic information dumped in Scraper/Lists.

        Method for overloading inits from http://stackoverflow.com/questions/141545/overloading-init-in-python

        :param list_collection_input:
        :return:
        """
        return cls(list_collection_input)

    @classmethod
    def fromsearchitem(cls, json_collection_input):
        """
        Caitlin Macleod u5351248
        07/10/2015

        Initialise a Collection object from the result that is given by the raw search json linked to at the top.

        Method for overloading inits from http://stackoverflow.com/questions/141545/overloading-init-in-python

        :param json_collection_input:
        :return:
        """
        data = dict()
        data['name'] = json_collection_input['Name']
        data['code'] = json_collection_input['SubPlanCode']
        data['type'] = json_collection_input['SubplanType']
        data['units'] = json_collection_input['Units']
        data['ignoreme'] = False
        data['filename'] = "TempDownloads/Collections/" + data['code'] + ".html"
        return cls(data)

    def download_data(self):
        """
        Caitlin Macleod u5351248
        06/10/2015

        Download the detailed html from this collection's page
        and store it in TempDownloads/Collections/<Code>.html

        :return: a tuple containing the path to the newly created
                 data file as well as the resulting HTTPMessage object.
        """
        url = "http://programsandcourses.anu.edu.au/"
        if self.type == "MAJ":
            url += "major/" + self.code
        elif self.type == "MIN":
            url += "minor/" + self.code
        elif self.type == "SPC":
            url += "specialisation/" + self.code
        try:
            print(" Done!")
            return request.urlretrieve(url, self.filename)
        except URLError:
            print(" Tried to download collection data from " + url +
                  " but failed. Setting " + self.code + " to be ignored.")
            self.ignoreme = True

    def parse_data(self, repo):
        """
        Caitlin Macleod u5351248
        06/10/2015

        Some code taken from Nick Sifniotis' Program.py

        Read through the information in the collection's raw html file and store that more usefully.

        :return: nil
        """
        with open("../Scraper/" + self.filename, "r") as myfile:
            stream = myfile.read().replace('\n', '')

        # fix the HR issue with BeautifulSoup by getting rid of the HRs
        stream = stream.replace('<hr>', '')
        stream = stream.replace('<HR>', '')

        raw_data = BeautifulSoup(stream, "html.parser")

        # todo: parse specialisations

        # threefold split
        if self.type == "MIN":
            for subplan_data in raw_data.findAll(class_="subplan-course-requirement-heading"):
                self.__parse_minor(subplan_data, repo)
        elif self.type == "MAJ":
            for subplan_data in raw_data.findAll(class_="subplan-course-requirement-heading"):
                self.__parse_minor(subplan_data, repo)

    def __find_unit_reqs(self, raw_data):
        """
        Nick Sifniotis u5809912
        28/10/2015

        Returns the number of units that this collection requires the completion of.

        :param raw_data: the HTML element with the class type subplan-course-requirement-heading
        :return: a new courselist object initialised to the correct number of units
        """
        if self.ignoreme:
            return -1

        res = None
        unprocessed_content = raw_data.getText()
        tokens = unprocessed_content.split(" ")
        if self.represents_int(tokens[0]):
            res = FiniteList(int(tokens[0]), "EXACT")
        elif self.represents_int(tokens[3]):
            res = FiniteList(int(tokens[3]), "MIN" if tokens[1] == "minimum" else "MAX")

        return res

    def __parse_minor(self, raw_data, repo):
        """
        Nick Sifniotis u5809912
        28/10/2015

        Parse the raw HTML and extract the course list data. Populate this minor object with it.

        :param raw_data: the raw HTML passed through BeautifulSoup.
        :return: nothing - the data is being added to this object itself.
        """
        if raw_data is None or self.ignoreme:
            self.ignoreme = True
            print("Unable to parse " + self.name + " automatically, manual intervention required. Code is " + self.code)
            return

        # the first line ought to be a p with class type subplan-course-requirement-heading
        if raw_data.name != "p":
            self.ignoreme = True
            print("Parse error on line " + str(raw_data) + ", expecting to see a p subplan-course-requirement-heading here.")
            return

        next_course_list = self.__find_unit_reqs(raw_data)

        # the next line ought to be the table.
        # there may be whitespace in the way though. Curse that whitespace.
        raw_data = raw_data.next_sibling
        if raw_data.name != "table":
            raw_data = raw_data.next_sibling

        if raw_data is None or raw_data.name != "table":
            self.ignoreme = True
            print("Parse error on line " + str(raw_data) + ", expecting to see a table here.")
            return

        # results now contain the table that contains the information we're after
        # the zero'th column of the table contains the list of course codes that make up this minor.
        for row in raw_data.tbody.findAll('tr'):
            first_column = row.findAll('td')[0].contents
            for item in first_column:
                next_course = None
                course_code = None

                if isinstance(item, Tag):
                    if item.name == 'a':
                        course_code = item.contents
                    elif item.name == 'p':
                        for small_item in item.findAll('a'):
                            course_code = small_item.contents

                # try to figure out which course this is for.
                if course_code:
                    course_code = course_code[0]        # because it returns a list of one
                    garbage, next_course = repo.identify_me(course_code, "CRS")
                    if next_course is not None:
                        next_course_list.add_course(course_code, repo)

        print(str(next_course_list))
        self.course_lists.append(next_course_list)

    def represents_int(self, s):
        """
        Nick Sifniotis u5809912
        28/10/2015

        Pinched off stackexchange.
        http://stackoverflow.com/questions/1265665/python-check-if-a-string-represents-an-int-without-using-try-except
        No prizes for guessing what I was googling.

        :param s: the mystery string
        :return: true if the string can safely be converted to an integer.
        """
        try:
            int(s)
            return True
        except ValueError:
            return False
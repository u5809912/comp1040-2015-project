__author__ = 'nsifniotis'

from urllib import request
from urllib.error import URLError
from bs4 import BeautifulSoup, NavigableString, Tag
from DataModel.ProgramConditional import *
from Scraper.ProgramTokeniser import Tokeniser


class Program:
    """
    Nick Sifniotis u5809912
    29/09/2015

    Contains everything that an ANU program needs to know about itself.

    The list of programs is downloaded by ProgramScraper from
    http://programsandcourses.anu.edu.au/data/ProgramSearch/GetPrograms?ShowAll=true&Careers[0]=Undergraduate

    Individual programs are then constructed from this data.

    """

    def __init__(self, data_source):
        """
        Nick Sifniotis u5809912
        29/09/2015

        Initialise from the dictionary object returned by the JSON parser.
        The conditions list will contain all of the restrictions imposed on this degree.

        :param data_source: - the object created from the 'list of programs' JSON data
        """
        self.program_name = data_source['program_name']
        self.program_code = data_source['program_code']
        self.program_length = data_source['program_length']
        self.filename = data_source['filename']
        self.ignore_me = self.program_name_filter(self.program_name)
        self.minimum_unit_requirement = 0
        self.data_repo = None

        self.conditions = None
        self.courses = []
        self.majors = []
        self.minors = []

        self.ignore_rules = []

    @classmethod
    def from_search_item(cls, search_item):
        """
        Nick Sifniotis u5809912
        27/10/2015

        Initialise this program from data scraped from the university's programs search script.

        :param search_item: the raw JSON data
        """
        data = dict()
        data['program_name'] = search_item['ProgramName']
        data['program_code'] = search_item['AcademicPlanCode']
        data['program_length'] = search_item['Duration']
        data['filename'] = "TempDownloads/Programs/" + search_item['AcademicPlanCode'] + ".html"
        return cls(data)

    @classmethod
    def from_json_list(cls, json_data):
        """
        Nick Sifniotis u5809912
        27/10/2015

        Initialise this program from data saved by the scraper in Scraper/Lists

        :param json_data: the JSON data to load from
        """
        return cls(json_data)

    @classmethod
    def program_name_filter(cls, program_name):
        """
        Nick Sifniotis u5809912
        07/10/2015

        There are some programs that this system is not designed to handle.

        These programs can be killed off simply by searching for keywords in their program names.
        They can be tracked down and eliminated before any heavy processing is done.

        This function performs that filtering task.
        @TODO: Honours courses cannot so lightly be tossed aside. The BAC RD Hns for example is not a 48 unit program
        :param program_name: the program name to check.
        :return: true if the program is to be rejected, false otherwise.
        """
        res = False

        excluded_programs = ["Flexible", "Diploma", "(Honours)", "/", "Flexible"]
        for exclusion in excluded_programs:
            res |= exclusion in program_name

        return res

    def download_data(self):
        """
        Nick Sifniotis u5809912
        29/09/2015

        Download and save the HTML file on the ANU P&C website
        that contains this program's data.
        """
        if self.ignore_me:
            print("\n Ignoring program " + self.program_name)
            return

        url = "http://programsandcourses.anu.edu.au/Program/" + self.program_code
        try:
            print(" Done!")
            return request.urlretrieve(url, self.filename)
        except URLError:
            print(" Tried to download program data from " + url +
                  " but failed. Setting " + self.program_code + " to be ignored.")
            self.ignore_me = True

    def filter_texts(self, input_data):
        """
        Nick Sifniotis u5809912
        06/10/2015

        Filter all the text and try to standardise it a little.
        :return: a list containing the slightly less rubbish texts.
        """

        res = []

        for item in input_data:
            if item is not "":
                item = item.strip()

                # replacement of non-standard tokens
                item = item.replace('\xa0', ' ')
                item = item.replace('.', '')
                item = item.replace(':', '')

                item = item.replace(" can ", " may ")           # spaces are there because 'american' became 'amerimay'
                item = item.replace("A further", "")
                item = item.replace("must include", "must consist of")
                item = item.replace("the completion of", "completion of")
                item = item.replace("from completion of", "from")
                item = item.replace("from the following", "from")
                item = item.replace("must come from", "from")
                item = item.replace("may come from", "from")
                item = item.replace("from a", "from")
                item = item.replace(" core ", " ")
                item = item.replace(" compulsory ", " ")
                item = item.replace(" from list", "")

                item = item.replace("A  minimum of", "Minimum")
                item = item.replace("maximum of", "maximum")
                item = item.replace("minimum of", "minimum")
                item = item.replace("A maximum", "Maximum")
                item = item.replace("A minimum", "Minimum")

                item = item.replace(" one ", " ")
                item = item.replace(" two ", " ")
                item = item.replace(" second ", " ")

                item = item.replace("units, of which", "units")
                item = item.replace("units, including", "units")
                item = item.replace("units, which must consist of", "units")
                item = item.replace("000 level", "000-level")
                item = item.replace("000- level", "000-level")
                item = item.replace("000-,", "000-level,")
                item = item.replace("000- ", "000-level ")
                item = item.replace("000, ", "000-level, ")
                item = item.replace("000 ", "000-level ")

                item = item.replace(" courses", " course")
                item = item.replace(" majors", " major")
                item = item.replace(" minors", " minor")
                item = item.replace(" specialisations", " specialisation")

                item = item.replace(" course", " CRS")
                item = item.replace(" major", " MAJ")
                item = item.replace(" minor", " MIN")
                item = item.replace(" specialisation", " SPC")

                item = item.replace(" the Science CRS list", " SCI-CRS-LIST")

                item = item.replace(" offered by ANU", "")
                item = item.replace(" of the following", "")
                item = item.replace(" workshop ", " ")

                item = self.data_repo.ANU.filter_item(item)
                item = self.remove_program_name(item)
                item = item.strip()

                if self.NUS_fail(item):
                    self.ignore_me = True
                elif self.ignore_rule(item):
                    self.ignore_rules.append(item)
                else:
                    res.append(item)

        return res

    def is_major(self, inputdata):
        parts = inputdata.split(' ')
        return parts[-1].lower() == 'major'

    def is_minor(self, inputdata):
        parts = inputdata.split(' ')
        return parts[-1].lower() == 'minor'

    def is_course_code(self, inputdata):
        if len(inputdata) < 8:
            return False

        if not inputdata[4].isdigit():
            return False
        if not inputdata[5].isdigit():
            return False
        if not inputdata[6].isdigit():
            return False
        if not inputdata[7].isdigit():
            return False

        return True

    def NUS_fail (self, inputdata):
        """
        Nick Sifniotis u5809912
        04/10/2015

        This function searches for rules that invalidate the entire program.

        :param inputdata:
        :return:
        """
        res = False
        if 'NUS' in inputdata:
            res = True
        if 'Diploma' in inputdata:
            res = True
        if 'National University of Singapore' in inputdata:
            res = True
        if 'vertical double' in inputdata:
            res = True
        if 'music' in inputdata.lower():
            res = True

        return res

    def ignore_rule(self, inputdata):
        tokens = inputdata.split(' ')
        if tokens[0] == 'Students':
            return True
        if "The minor in Business and Economics Essentials" in inputdata:
            return True
        if tokens[0] == "To":
            return True
        if tokens[0] == "If":
            return True
        if tokens[0] == "In":
            return True
        if tokens[0] == "For":
            return True
        if 'units must consist of' in inputdata:
            return True
        if 'Final Honours Grade' in inputdata:
            return True
        if 'may contribute towards meeting' in inputdata:
            return True

        return False

    def remove_program_name(self, inputdata):
        """
        Nick Sifniotis u5809912

        Removes the program name, if it's there, from the input string. So 'The Bachelor of Information Technology requires .."
        will become "This program requires .." for example.

        :param inputdata: the input text
        :return: the output text (WOW)
        """
        tokens = inputdata.split(' ')

        if len(tokens) < 3:
            return inputdata

        if (tokens[0] != "The" and tokens[0] != 'This') or tokens[1] != "Bachelor":
            return inputdata

        res = "This program "
        location = 0
        for i in range (len(tokens)):
            if tokens[i] == "requires":
                location = i

        res += " ".join(tokens[location:])

        return res

    def process_numeric(self, inputdata):
        # two tranches of rules.
        # bifurcate on letters 1, 2 and 3
        if inputdata[1:3] == "000":
            # it's gonna be 3000 level COMP or 4000 level COMP
            self.courses.append("[COMP " + inputdata[0:3] + "]")
        else:
            self.courses.append("Unit restriction rule: " + inputdata)

    def parse_data(self, repo):
        """
        Nick Sifniotis u5809912
        29/09/2015

        Parse the data that was downloaded and saved.

        I've split the download from the parse so I can work with the entire dataset without having to redownload
        it each time.

        :return: dictionary of rules which cannot be processed with rule as key for value 1
        """
        self.data_repo = repo

        with open(self.filename, "r") as myfile:
            stream = myfile.read().replace('\n', '')

        # fix the HR issue with BeautifulSoup by getting rid of the HRs
        stream = stream.replace('<hr>', '')
        stream = stream.replace('<HR>', '')
        stream = stream.replace('<br>', "\n")
        stream = stream.replace('<BR>', "\n")

        raw_data = BeautifulSoup(stream, "html.parser")


        # get the number of units
        self.parse_unit_requirement(raw_data.find(class_="degree-summary__requirements-units"))
        half_parse = self.analyse_structure(raw_data.find(id="program-requirements"))

        # get the text out of the HTML
        results = []
        for item in half_parse:
            if isinstance(item, Tag):
                results.append(item.get_text())
            elif isinstance(item, NavigableString):
                results.append(item)

        # there might be line breaks in an item. They need to be split.
        more_results = []
        for item in results:
            parts = item.split("\n")
            for bit in parts:
                more_results.append(bit)

        results = self.filter_texts(more_results)

        if self.ignore_me:
            return


        print()
        print("***************************")
        print("Program Name: " + self.program_name + " (" + self.program_code + ")")
        print()

        tk = Tokeniser(results)
        self.get_top_rule(tk)

        item = tk.get()
        while item is not None:
            print(item)
            tk.next()
            item = tk.get()

        return

        for item in results:
            if item is not "":
                if item is None or item == "":
                    t = 1
                elif self.NUS_fail(item):
                    self.ignore_me = True
                elif self.is_major(item):
                    #print ("major:: " + item)
                    self.majors.append(item)
                elif self.is_minor(item):
                    #print ("minor:: " + item)
                    self.minors.append(item)
                elif self.is_course_code(item):
                    self.courses.append(item)
                elif self.ignore_rule(item):
                    #print (item)
                    self.ignore_rules.append(item)

            #    elif item[0].isdigit():
            #        self.process_numeric(item)
                else:
                    dic[item] = 1

        return dic

    def parse_unit_requirement(self, raw_data):
        """
        Nick Sifniotis u5809912
        29/09/2015

        Extract the total number of units that make up this program.

        :param raw_data: The portion of HTML code that contains the information that we are looking for
        :return: nothing - the data is saved as part of this object's state.
        """
        if raw_data is None:
            return

        raw_data.span.extract()

        for line in raw_data:
            line = line.strip()
            if line:
                # format is XXX Units
                # so kill off the ' Units'
                self.minimum_unit_requirement = int(line.replace(" Units", ""))

    def describe_yourself(self):
        """
        Nick Sifniotis u5809912
        29/09/2015

        Dump to screen to make sure the data is being parsed correctly.

        :return:
        """
        print(self.program_code + ": " + self.program_name + ", " + str(self.program_length) +
               " years (" + str(self.minimum_unit_requirement) + " units)")
        print("Courses identified: " + str(self.courses))
        print("Majors identified: " + str(self.majors))
        print("Minors identified: " + str(self.minors))
        print("Ignored rules: " + str(self.ignore_rules))


    def analyse_structure(self, raw_data):
        """
        Nick Sifniotis u5809912
        29/09/2015

        First pass through the data. Simply return the HTML tags that contain the data we want.

        :param raw_data:
        :return: a list of tags that contain the 'program requirements'
        """
        if raw_data is None:
            return []

        results = []

        finished = False
        while not finished:
            raw_data = raw_data.next_sibling
            if raw_data.name == 'h2':
                finished = True
            else:
                results.append(raw_data)

        return results


    def parse_a_href(self, item):
        """
        Nick Sifniotis u5809912
        30/09/2015

        Parse the href of an a tag and [eventually] link it to the course/major/whatever object

        :param item: the A tag to deal with
        :return: the correctly parsed payload..
        """
        res = ""
        href = item['href'].replace("http://programsandcourses.anu.edu.au/", '').lower()
        atype = ""
        if "lists" in href:
            atype = ""
        elif "course" in href:
            atype = "COURSE"
        elif "major" in href:
            atype = "MAJOR"
        elif "minor" in href:
            atype = "MINOR"

        if not type:
            res = item['href']
        else:
            bits = href.split ("/")
            code = bits[-1].upper()
            res = atype + ":" + code         #@TODO eventally this must link to the object itself

        return res


    def get_top_rule(self, tk):
        """
        Nick Sifniotis u5809912
        06/10/2015

        Search through the top-level rules until one is found that doesn't need to be ignored.

        :param tk: - the program data 'tokeniser'
        :return:
        """
        item = tk.get()
        tk.next()

        if item is None:
            return None

        # what is the nature of the rule?
        if "This program requires" in item:
            # kewl
            # how many units then?
            parts = item.split(' ')
            num_units = parts[-2]       # I can't believe how useful negative indices are.

            print ("TOTAL REQD: " + num_units)

            res = self.get_this_program_requires_branch(tk)
            while res is not None:
                res = self.get_this_program_requires_branch(tk)
        else:
            # GODDAMNIT
            res = None      # @TODO more rules later
            print ("TOP LEVEL PARSE FAILURE: " + item)

        return res


    def get_this_program_requires_branch(self, tk):
        """
        Nick Sifniotis u5809912
        06/10/2015

        Dear god

        :param tk:
        :return:
        """
        item = tk.get()
        res = None

        if item is None:
            return res

        rule_type = "EXACT"
        if 'Maximum' in item:
            rule_type = "MAX"
            item = item.replace("Maximum ", "")

        if "Minimum" in item:
            rule_type = "MIN"
            item = item.replace("Minimum ", "")

        if item[0].isdigit():
            # it is likely that a list of course/major/minor/specialisations will follow
            # try to ascertain which one it is from the text
            tk.next()
            holding = []
            parts = item.split(' ')
            if 'CRS' in parts[3]:
                holding = self.get_courses(tk)
            elif 'MAJ' in parts[3]:
                holding = self.get_majors(tk)
            elif 'MIN' in parts[3]:
                holding = self.get_minors(tk)
            elif parts[3] == 'one' or parts[3] == 'two':
                if parts[-1] == 'MAJ':
                    holding = self.get_majors(tk)
                elif parts[-1] == 'SPC':
                    holding = self.get_specialisations(tk)
                elif parts[-1] == 'MIN':
                    holding = self.get_minors(tk)
                else:
                    print("Parsing error in (one or two) subsection: " + item)
            elif parts[3] == 'the':
                thing_name = " ".join(parts[4:len(parts) - 1])
                thing = None
                if parts[-1] == 'MAJ':
                    if thing_name in self.data_repo.majors:
                        thing = self.data_repo.majors[thing_name]
                elif parts[-1] == 'MIN':
                    if thing_name in self.data_repo.minors:
                        thing = self.data_repo.minors[thing_name]
                elif parts[-1] == 'SPC':
                    if thing_name in self.data_repo.specialisations:
                        thing = self.data_repo.specialisations[thing_name]
                else:
                    # this shit ain't right
                    print ("PARSE ERROR!!: " + parts[-1] + " found in unknown string " + item)
                    return

                if thing is None:
                    # we have a problem finding out what this is
                    print("Unable to correctly parse thing '" + thing_name + "' in item " + item)
                else:
                    holding.append(thing['code'])
            elif '-level' in parts[3]:
                for part in parts[3:]:
                    if '-' in part:
                        dashy = part.split('-')
                        holding.append(dashy[0])

            num_units = parts[0]

            print (rule_type + " ITEM " + item)
            print (rule_type + " " + num_units + " FROM " + str(holding))
            res = ExactLeaf(num_units, holding)

        elif item == 'Either':
            # at least we know exactly what to do here
            tk.next()

            res = ORTreeNode()

            res.left_node = self.get_this_program_requires_branch(tk)

            tk.next()
            if tk.get().lower() != "or":
                print ("FUcK")

            res.right_node = self.get_this_program_requires_branch(tk)

            print (res.left_node)
            print (res.right_node)
        else:
            tk.next()
            print(item)

        return res


    def get_specialisations(self, tk):
        res = []
        finished = False

        while not finished:
            item = tk.get()

            if item is not None:
                if item in self.data_repo.specialisations:
                    res.append(self.data_repo.specialisations[item]['code'])
                    tk.next()
                else:
                    if (item + " Honours") in self.data_repo.specialisations:
                        res.append(self.data_repo.specialisations[item + " Honours"]['code'])
                        tk.next()
                    else:
                        finished = True
            else:
                finished = True

        return res


    def get_courses(self, tk):
        """
        This is actually an absurdly simple test for courses. Course codes are simply combinations of
        capital letters and numbers and no other thing - namely, no lowercase letters.
        :param tk:
        :return:
        """

        res = []
        finished = False

        while not finished:
            item = tk.get()

            if item is not None:
                parts = item.split(' ')
                for part in parts[0]:
                    finished = finished or part.islower()

                finished = finished or len(parts[0]) < 7
            else:
                finished = True

            if not finished:
                res.append(parts[0])
                tk.next()

        return res


    def get_lists(self, tk):
        return ['lists']


    def get_majors(self, tk):
        res = []
        finished = False

        while not finished:
            item = tk.get()

            if item is not None:
                if item in self.data_repo.majors:
                    res.append(self.data_repo.majors[item]['code'])
                    tk.next()
                else:
                    if (item + " Major") in self.data_repo.majors:
                        res.append(self.data_repo.majors[item + " Major"]['code'])
                        tk.next()
                    else:
                        finished = True
            else:
                finished = True

        return res


    def get_minors(self, tk):
        res = []
        finished = False

        while not finished:
            item = tk.get()

            if item is not None:
                if item in self.data_repo.minors:
                    res.append(self.data_repo.minors[item]['code'])
                    tk.next()
                else:
                    if (item + " Minor") in self.data_repo.minors:
                        res.append(self.data_repo.minors[item + " Minor"]['code'])
                        tk.next()
                    else:
                        finished = True
            else:
                finished = True

        return res

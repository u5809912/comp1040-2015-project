__author__ = 'nsifniotis'


def check_for_exclusions(course, course_list):
    """
    Nick Sifniotis u5809912
    27/10/2015

    Checks a list of courses against the exclusions and incompatibles of a particular course, to make sure that
    the course list does not contain a course that the course is incompatible with.

    :param course: the course who's exclusions and incompatibilies are being checked
    :param course_list: the list of courses to check against those exclusions
    :return: true if an incompatibility is found, false otherwise.
    """
    result = False

    for excluded_course in course.prerequisites['incompatible']:
        if course_in(course_list, excluded_course):
            result = True

    return result


def course_in(course_list, course_to_find):
    """
    Nick Sifniotis u5809912
    27/10/2015

    Searches for course_to_find in a list of courses, and returns true if a match is found.
    @TODO: Right now this works only for 'solo' courses, not infinite lists. Make it work for both.

    :param course_list: the list of courses to check
    :param course_to_find: the course to find
    :return: true if the course is found in course_list
    """
    for course in course_list:
        if course == course_to_find:
            return True

    return False

__author__ = 'nsifniotis'


class ProgramTreeNode:

    def validate(self):
        """
        Nick Sifniotis u5809912
        06/10/2015

        As this is the base class, this method will do nothing.

        No instance of ProgramTreeNode should ever be created.

        :return: false, just because.
        """
        return False


class ANDTreeNode(ProgramTreeNode):

    def __init__(self):
        self.left_node = None
        self.right_node = None

    def validate(self):
        """
        Nick Sifniotis u5809912
        06/10/2015

        :return: true if both of my subbranches return true, false otherwise
        """
        if (self.left_node is None) or (self.right_node is None):
            return False

        return self.left_node.validate(self) and self.right_node.validate(self)


class ORTreeNode(ProgramTreeNode):

    def __init__(self):
        self.left_node = None
        self.right_node = None

    def validate(self):
        """
        Nick Sifniotis u5809912
        06/10/2015

        :return: true if both of my subbranches return true, false otherwise
        """
        if (self.left_node is None) or (self.right_node is None):
            return False

        return self.left_node.validate(self) or self.right_node.validate(self)


class MinimumLeaf(ProgramTreeNode):
    """
    Nick Sifniotis u5809912
    06/10/2015

    Validates true if and only if - actually, I don't know, because the validation function will depend on
    what question is being asked.
    However, it will accept a minimum number of units and a list of courses that those units will have to come from.
    """

    def __init__(self, num_units, courses):
        self.num_units = num_units
        self.course_list = courses

    def validate(self):
        return True


class MaximumLeaf(ProgramTreeNode):
    """
    Nick Sifniotis u5809912
    06/10/2015

    Validates true if and only if - actually, I don't know, because the validation function will depend on
    what question is being asked.
    However, it will accept a minimum number of units and a list of courses that those units will have to come from.
    """

    def __init__(self, num_units, courses):
        self.num_units = num_units
        self.course_list = courses

    def validate(self):
        return True


class ExactLeaf(ProgramTreeNode):
    """
    Nick Sifniotis u5809912
    06/10/2015

    Validates true if and only if - actually, I don't know, because the validation function will depend on
    what question is being asked.
    However, it will accept a minimum number of units and a list of courses that those units will have to come from.
    """

    def __init__(self, num_units, courses):
        self.num_units = num_units
        self.course_list = courses

    def validate(self):
        return True
